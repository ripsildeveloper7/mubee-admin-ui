 // This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


 
export const environment = {
  production: true,
  //contentServiceUrl: "http://localhost:3110/",
  // commerceOrderServiceUrl: 'http://localhost:3112/',
  // customerServiceUrl: "http://localhost:3111/",
  // marketingServiceUrl: 'http://localhost:3117/',

  // imageUploadServiceUrl:  'http://localhost:3200/',
  // productServiceUrl: 'http://localhost:3072/',

  cmsServiceUrl: 'https://hos0hp8j8e.execute-api.ap-south-1.amazonaws.com/qa/',
  contentServiceUrl: 'https://hos0hp8j8e.execute-api.ap-south-1.amazonaws.com/qa/',
  customerServiceUrl: 'https://3mek1zbxme.execute-api.ap-south-1.amazonaws.com/qa/',
  commerceOrderServiceUrl: 'https://m317jtnwx8.execute-api.ap-south-1.amazonaws.com/qa/',
  marketingServiceUrl: 'https://7b36xythn5.execute-api.ap-south-1.amazonaws.com/qa/',
  imageUploadServiceUrl:  'https://ha0h1va6a0.execute-api.ap-south-1.amazonaws.com/qa/',
  productServiceUrl: 'https://euwinsi5x6.execute-api.ap-south-1.amazonaws.com/qa/',  

  instagramUrl : 'https://api.instagram.com/v1/users/self/media/recent/?access_token=8636464595.f5aef9d.b112bf6c1e6a41a482ec7e7a77882469',
  productImageUrl: 'https://mubee-product-images.s3.ap-south-1.amazonaws.com/images/product',
  sizeGuideImageUrl: 'https://mubee-product-images.s3.ap-south-1.amazonaws.com/images/size/',
  categoryImageUrl: 'https://mubee-product-images.s3.ap-south-1.amazonaws.com/images/category/',
  contentImageUrl: 'https://mubee-content-images.s3.ap-south-1.amazonaws.com/images/',
  subCategoryImageUrl: 'https://mubee-product-images.s3.ap-south-1.amazonaws.com/images/subcategory/',
  mainCategoryBannerImageUrl: 'https://mubee-product-images.s3.ap-south-1.amazonaws.com/images/maincategory/',
  brandImageUrl: 'https://mubee-product-images.s3.ap-south-1.amazonaws.com/images/brand/',
  categoryBannerImageUrl: 'https://mubee-product-images.s3.ap-south-1.amazonaws.com/images/categorybanner/',
  excelUrl:  'https://mubee-product-images.s3.ap-south-1.amazonaws.com/excel/',
  // vendorImageServiceUrl: 'https://mubee-product-images.s3.ap-south-1.amazonaws.com/images/',
  parentResizeId: 1,
  childResizeId: 2,
  limitResize: 500,
  appId: 'mubee'
};






//  export const environment = {
//     production: false,
//     productServiceUrl: "http://localhost:3072/",
    
//     productImageUrl: "https://studentbus.in-product-images.s3.ap-south-1.amazonaws.com/images/product/",
//     subCategoryImageUrl: "https://studentbus.in-product-images.s3.ap-south-1.amazonaws.com/images/subcategory/",
//     brandImageUrl: "https://studentbus.in-product-images.s3.ap-south-1.amazonaws.com/images/brand/",
//     customerServiceUrl: "http://localhost:3111/",
//     commerceOrderServiceUrl: "http://localhost:3112/",
//     imageUploadServiceUrl:  'http://localhost:3200/',
//     marketingServiceUrl: "http://localhost:3117/",
//     contentServiceUrl: "http://localhost:3110/",
//     categoryImageUrl: "https://studentbus.in-product-images.s3.ap-south-1.amazonaws.com/images/category/",
//     sizeGuideImageUrl: "https://studentbus.in-product-images.s3.ap-south-1.amazonaws.com/images/size/",
//     instagramUrl: "https://api.instagram.com/v1/users/self/media/recent/?access_token=3113801366.cb9df17.c54db3dc31c046c18e33bd1167ab6a3e",
//     categoryBannerImageUrl: 'https://studentbus.in-product-images.s3.ap-south-1.amazonaws.com/images/categorybanner/',
//     excelUrl:  'https://studentbus.in-product-images.s3.ap-south-1.amazonaws.com/excel/',
//     // vendorImageServiceUrl: 'https://studentbus.in-product-images.s3.ap-south-1.amazonaws.com/images/',
//     mainCategoryBannerImageUrl: 'https://studentbus.in-product-images.s3.ap-south-1.amazonaws.com/images/maincategory/',
//     appId: 'studentbus.in'
//   };
 
 /* * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
