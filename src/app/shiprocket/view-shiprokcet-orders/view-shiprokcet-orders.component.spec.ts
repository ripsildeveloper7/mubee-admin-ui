import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewShiprokcetOrdersComponent } from './view-shiprokcet-orders.component';

describe('ViewShiprokcetOrdersComponent', () => {
  let component: ViewShiprokcetOrdersComponent;
  let fixture: ComponentFixture<ViewShiprokcetOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewShiprokcetOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewShiprokcetOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
