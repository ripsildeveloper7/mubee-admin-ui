import { Component, OnInit } from '@angular/core';
import { ShiprocketService } from '../shiprocket.service';
import { Route, ActivatedRoute, Router } from '@angular/router'
@Component({
  selector: 'app-view-shiprokcet-orders',
  templateUrl: './view-shiprokcet-orders.component.html',
  styleUrls: ['./view-shiprokcet-orders.component.css']
})
export class ViewShiprokcetOrdersComponent implements OnInit {
  shiprocketOrders;
  pdfUrl;
  constructor(private shiprocketService: ShiprocketService, private router: Router) { }

  ngOnInit() {
    this.getAllOrders();
  }
  getAllOrders() {
    this.shiprocketService.getAllShiprocketOrders().subscribe(data => {
      this.shiprocketOrders = data.data;
      console.log(data, ' order details');

    }, err => {
      console.log(err);
    })
  }
  createInvoice(id) {
    this.shiprocketService.createInvoice(id).subscribe(data => {
      /*   this.pdfUrl = data.invoice_url */
      console.log(data, 'Invoice creation');
    }, err => {
      console.log(err);
    })
  }
  shipNow(id) {
    this.router.navigate(['shiprocket/courierservice', id])
  }
  cancelOrder(id) {
    this.shiprocketService.cancelShiprocketOrder(id).subscribe(data => {
      console.log(data);
    }, err => {
      console.log(err);
    });
  }
  downloadInvoice(id) {
    this.shiprocketService.downloadInvoice(id).subscribe(data => {
      this.pdfUrl = data.invoice_url;
      console.log(data);
    }, err => {
      console.log(err);
    });
  }
  viewSingleOrder(id) {
    console.log(id, 'singleorder')
    this.router.navigate(['shiprocket/singleorder', id])
  }
  trackByAwb(id) {
   /*  console.log('awb no', id); */
    this.router.navigate(['shiprocket/trackorder', id])
    this.shiprocketService.trackByAwb(id).subscribe(data => {
      console.log(data);
    }, err => {
      console.log(err);
    })
  }
  trackShipmentId(id) {
    this.shiprocketService.trackByShipment(id).subscribe(data => {
      console.log(data);
    }, err => {
      console.log(err);
    })
  }
}
