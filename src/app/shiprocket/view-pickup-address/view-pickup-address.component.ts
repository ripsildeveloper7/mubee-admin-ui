import { Component, OnInit } from '@angular/core';
import {ShiprocketService} from '../shiprocket.service';

@Component({
  selector: 'app-view-pickup-address',
  templateUrl: './view-pickup-address.component.html',
  styleUrls: ['./view-pickup-address.component.css']
})
export class ViewPickupAddressComponent implements OnInit {
  pickupAddressDetails;

  constructor(private shiprocketService: ShiprocketService) { }

  ngOnInit() {
    this.getAllPickUpAddress();
  }

  getAllPickUpAddress() {
    this.shiprocketService.getPickUpAddress().subscribe(data => {
      this.pickupAddressDetails = data.data.shipping_address;
    }, err => {
      console.log(err);
    })

  }
}
