import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPickupAddressComponent } from './view-pickup-address.component';

describe('ViewPickupAddressComponent', () => {
  let component: ViewPickupAddressComponent;
  let fixture: ComponentFixture<ViewPickupAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPickupAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPickupAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
