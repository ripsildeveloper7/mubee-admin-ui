import { Component, OnInit } from '@angular/core';
import { ShiprocketService } from '../shiprocket.service';

@Component({
  selector: 'app-get-channel',
  templateUrl: './get-channel.component.html',
  styleUrls: ['./get-channel.component.css']
})
export class GetChannelComponent implements OnInit {
channelDetails;
  constructor(private shiprocketService: ShiprocketService) { }
  ngOnInit() {
    this.getChannelDetails();
  }

  getChannelDetails() {
    this.shiprocketService.getChannels().subscribe(data => {
      console.log(data, 'get channel details');
      this.channelDetails = data.data;
    }, err => {
      console.log(err);
    })
  }
}
