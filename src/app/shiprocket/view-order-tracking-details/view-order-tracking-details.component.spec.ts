import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewOrderTrackingDetailsComponent } from './view-order-tracking-details.component';

describe('ViewOrderTrackingDetailsComponent', () => {
  let component: ViewOrderTrackingDetailsComponent;
  let fixture: ComponentFixture<ViewOrderTrackingDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewOrderTrackingDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewOrderTrackingDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
