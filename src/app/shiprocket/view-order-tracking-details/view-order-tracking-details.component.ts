import { Component, OnInit } from '@angular/core';
import { ShiprocketService } from '../shiprocket.service';
import { Route, ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: 'app-view-order-tracking-details',
  templateUrl: './view-order-tracking-details.component.html',
  styleUrls: ['./view-order-tracking-details.component.css']
})
export class ViewOrderTrackingDetailsComponent implements OnInit {

  trackingDetails;
  id;
  constructor(private shiprocketService: ShiprocketService, private router: Router,  private route: ActivatedRoute) {
    this.id = this.route.snapshot.params.orderid;
   }

  ngOnInit() {
    this.getTrackingDetails();
  }
  getTrackingDetails() {
    this.shiprocketService.trackByAwb(this.id).subscribe(data => {
      this.trackingDetails = data;
      console.log(data, ' order details');
    }, err => {
      console.log(err);
    })
  }

}
