import { Component, OnInit } from '@angular/core';
import { ShiprocketService } from '../shiprocket.service';
import { Route, ActivatedRoute, Router } from '@angular/router';
import { CourierService } from './courier-service.model';

@Component({
  selector: 'app-check-courier-serviceability',
  templateUrl: './check-courier-serviceability.component.html',
  styleUrls: ['./check-courier-serviceability.component.css']
})
export class CheckCourierServiceabilityComponent implements OnInit {
  id;
  singleOrderDetails;
  courierServices;
  courierServiceModel: CourierService;
  awbData;
  selectedCourierDetails;
  courierId;
  shipmentId;
  orderId;
  constructor(private shiprocketService: ShiprocketService, private route: ActivatedRoute) {
    this.id = this.route.snapshot.params.orderid;
  }

  ngOnInit() {
    this.viewSingleOrder();
  }

  viewSingleOrder() {
    this.shiprocketService.getSingleOrder(this.id).subscribe(data => {
      this.singleOrderDetails = data.data;
      this.shipmentId = this.singleOrderDetails.shipments.id;
      this.orderId = this.singleOrderDetails.id;
      console.log('single order details', this.singleOrderDetails);
      this.courierServiceModel = new CourierService();
      this.courierServiceModel.pickup_postcode = this.singleOrderDetails.pickup_code;
      this.courierServiceModel.delivery_postcode = this.singleOrderDetails.delivery_code;
      this.courierServiceModel.order_id = this.singleOrderDetails.id;
      this.courierServiceModel.cod = this.singleOrderDetails.cod;
      this.checkCourierService();
    }, err => {
      console.log(err);
    })
  }
  checkCourierService() {
    this.shiprocketService.checkCourierService(this.courierServiceModel).subscribe(data => {
      this.courierServices = data.data;
      console.log(data)
      this.courierServices.available_courier_companies.forEach(element => {
        if (element.courier_company_id === this.courierServices.shiprocket_recommended_courier_id) {
          this.selectedCourierDetails = element;
        }
      });
    }, err => {
      console.log(err);
    })
  }
  selectedCourier(id) {
    this.courierId = id;
    this.courierServices.available_courier_companies.forEach(element => {
      if (element.courier_company_id === id) {
        this.selectedCourierDetails = element;
      }
    });
  }
  generateAwb() {
    this.shiprocketService.generateAwb(this.shipmentId, this.courierId).subscribe(data => {
      this.awbData = data;
      console.log(data);
      this.updateAWB();
    }, err => {
      console.log(err);
    })
  }
  updateAWB() {
    this.shiprocketService.updateAWB(this.orderId, this.awbData).subscribe(data => {
      console.log('awb updated successfully');
      console.log(data);
    }, err => {
      console.log(err);
    })
  }
}
