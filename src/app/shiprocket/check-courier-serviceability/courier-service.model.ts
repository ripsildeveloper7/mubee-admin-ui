export class CourierService {
    pickup_postcode: Number;
    delivery_postcode: Number;
    order_id: string;
    cod: boolean;
}