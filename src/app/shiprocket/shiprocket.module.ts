import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule} from '@angular/forms';
import {
  MatSidenavModule,
  MatListModule,
  MatTooltipModule,
  MatOptionModule,
  MatSelectModule,
  MatMenuModule,
  MatSnackBarModule,
  MatGridListModule,
  MatToolbarModule,
  MatIconModule,
  MatButtonModule,
  MatRadioModule,
  MatCheckboxModule,
  MatCardModule,
  MatProgressSpinnerModule,
  MatExpansionModule,
  MatRippleModule,
  MatDialogModule,
  MatChipsModule,
  MatInputModule,
  MatFormFieldModule,
  MatStepperModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatTableModule,
  MatSortModule,
  MatTabsModule
} from '@angular/material';

import { ShiprocketRoutingModule } from './shiprocket-routing.module';
import { GetChannelComponent } from './get-channel/get-channel.component';
import {ShiprocketService} from './shiprocket.service';
import { ViewPickupAddressComponent } from './view-pickup-address/view-pickup-address.component';
import { CreateCustomOrderComponent } from './create-custom-order/create-custom-order.component';
import { ViewShiprokcetOrdersComponent } from './view-shiprokcet-orders/view-shiprokcet-orders.component';
import { CheckCourierServiceabilityComponent } from './check-courier-serviceability/check-courier-serviceability.component';
import { ViewSingleShiprocketOrderComponent } from './view-single-shiprocket-order/view-single-shiprocket-order.component';
import { ViewOrderTrackingDetailsComponent } from './view-order-tracking-details/view-order-tracking-details.component';

@NgModule({
  declarations: [GetChannelComponent, ViewPickupAddressComponent, CreateCustomOrderComponent, ViewShiprokcetOrdersComponent, CheckCourierServiceabilityComponent, ViewSingleShiprocketOrderComponent, ViewOrderTrackingDetailsComponent],
  imports: [
    CommonModule,
    ShiprocketRoutingModule,
    ReactiveFormsModule,
    MatCardModule,
    MatIconModule,
    MatTooltipModule,
    MatToolbarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatSelectModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatListModule,
    MatExpansionModule,
    MatMenuModule,
    MatStepperModule,
    MatDatepickerModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatTableModule,
    MatCheckboxModule,
    MatTabsModule
  ],
  providers: [
    ShiprocketService
  ]
})
export class ShiprocketModule { }
