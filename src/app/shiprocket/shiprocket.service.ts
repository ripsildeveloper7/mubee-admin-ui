import { Injectable } from '@angular/core';
import {AppSetting} from '../config/appSetting';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import {CustomerModel} from '../sales/view-single-order/customer.model';

@Injectable({
  providedIn: 'root'
})
export class ShiprocketService {
  commerceOrderServiceUrl: string = AppSetting.commerceOrderServiceUrl;
  customerServiceUrl: string = AppSetting.customerServiceUrl;
  constructor(private httpClient:HttpClient) { }

// get channel details
  getChannels(): Observable<any> {
    const salesUrl = 'getchannels';
    const url: string = this.commerceOrderServiceUrl + salesUrl;
    return this.httpClient.get<any>(url);
  }

  // get all pickup address

  getPickUpAddress(): Observable<any> {
    const salesUrl = 'getpickupaddress';
    const url: string = this.commerceOrderServiceUrl + salesUrl;
    return this.httpClient.get<any>(url);
  }

  // get single order details 
  getSingleOrderDetails(id): Observable<any> {
    const salesUrl = 'singleorders/';
    const url: string = this.commerceOrderServiceUrl + salesUrl + id;
    return this.httpClient.get<any>(url);
  }

  getSingleCustomer(id): Observable<any> {                     // Retrieve Single Customer
    const categoryUrl = 'getcustomerprofile/';
    const url: string = this.customerServiceUrl + categoryUrl + id;
    return this.httpClient.get<CustomerModel>(url);
  }
  createCustomOrder(orderDetails): Observable<any> {                     // Retrieve custom Customer
    const categoryUrl = 'customorder';
    const url: string = this.commerceOrderServiceUrl + categoryUrl ;
    return this.httpClient.post<CustomerModel>(url, orderDetails);
  }
  getAllShiprocketOrders(): Observable<any> {  // retreive shiprcoket orders
    const salesUrl = 'getshiprocketorder';  
    const url: string = this.commerceOrderServiceUrl + salesUrl;
    return this.httpClient.get<any>(url);
  }
  createInvoice(id): Observable<any> {  // retreive shiprcoket orders
    const salesUrl = 'createinvoice/';  
    const url: string = this.commerceOrderServiceUrl + salesUrl + id;
    return this.httpClient.get<any>(url);
  }
  
  getSingleOrder(id): Observable<any> {  // retreive single order in shiprocket
    const salesUrl = 'singleorder/';  
    const url: string = this.commerceOrderServiceUrl + salesUrl + id;
    return this.httpClient.get<any>(url);
  }
  checkCourierService(courierdetails): Observable<any> {  // check courier service for single order
    const salesUrl = 'courier';  
    const url: string = this.commerceOrderServiceUrl + salesUrl;
    return this.httpClient.post<any>(url, courierdetails);
  }
  createShiprocketOrder(orderDetails): Observable<any> {                     // create shiprocket order
    const categoryUrl = 'shiprocketorder';
    const url: string = this.commerceOrderServiceUrl + categoryUrl ;
    return this.httpClient.post<CustomerModel>(url, orderDetails);
  }

  cancelShiprocketOrder(id): Observable<any> {  // cancel single order in shiprocket
    const salesUrl = 'externalorder/cancel/';  
    const url: string = this.commerceOrderServiceUrl + salesUrl + id;
    return this.httpClient.get<any>(url);
  }

  downloadInvoice(id): Observable<any> {  // download invoice
    const salesUrl = 'downloadinvoice/';  
    const url: string = this.commerceOrderServiceUrl + salesUrl + id;
    return this.httpClient.get<any>(url);
  }

  generateAwb(shipmentid, courierid): Observable<any> {  // generate awb 
    const salesUrl = 'generate/';  
    const salesUrl2 = '/awb/';
    const url: string = this.commerceOrderServiceUrl + salesUrl + shipmentid + salesUrl2 + courierid;
    return this.httpClient.get<any>(url);
  }
  
  printLabel(id): Observable<any> {  // print label
    const salesUrl = 'printlabel/';  
    const url: string = this.commerceOrderServiceUrl + salesUrl + id;
    return this.httpClient.get<any>(url);
  }
  requestPickup(id): Observable<any> {  // ready to pickup
    const salesUrl = 'readytopickup/';  
    const url: string = this.commerceOrderServiceUrl + salesUrl + id;
    return this.httpClient.get<any>(url);
  }
  trackByAwb(id): Observable<any> {  // track  by awb
    const salesUrl = 'awb/';  
    const url: string = this.commerceOrderServiceUrl + salesUrl + id;
    return this.httpClient.get<any>(url);
  }
  trackByShipment(id): Observable<any> {  // track  by shipment id
    const salesUrl = 'trackshipment/';  
    const url: string = this.commerceOrderServiceUrl + salesUrl + id;
    return this.httpClient.get<any>(url);
  }
  updateAWB(id, awbNo): Observable<any> {  // update awb
    const salesUrl = 'trackshipment/';  
    const salesUrl1= '/awb/';
    const url: string = this.commerceOrderServiceUrl + salesUrl + id + salesUrl1 + awbNo;
    return this.httpClient.get<any>(url);
  }
}
