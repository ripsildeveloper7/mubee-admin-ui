export class CustomOrder {
  billing_phone: string;
    billing_customer_name: string;
    billing_last_name: string;
    billing_email: string;
      billing_address: string;
      billing_address_2: string;
      billing_pincode: string;
      billing_city: string;
      billing_state: string;
      billing_country: string;
      order_id: string;
      order_date: Date;
      channel_id: string;
      shipping_is_billing: boolean;
   /*    order_items: [{
        name: string;
        sku: string,
        units: Number,
        hsn: Number,
        tax: string,
        selling_price: string,
        discount: string

      }]; */
      order_items: any;
      payment_method: string;
      pickup_location: string;
      weight: Number;
      height: Number;
      breadth: Number;
      length: Number;
      sub_total: Number;
      total_discount: Number;
      shipping_charges: Number;
      giftwrap_charges: Number;
      transaction_charges: Number;
}
