import { Component, OnInit } from '@angular/core';
import { ShiprocketService } from '../shiprocket.service';
import { Route, ActivatedRoute, Router } from '@angular/router';
import { Order } from '../../sales/orders/order.model';
import { FormControl, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { CommentStmt } from '@angular/compiler';
import { CustomOrder } from './custom.order.model';
import { ShiprocketOrder } from '../../shared/model/shiprocketOrder.model'

@Component({
  selector: 'app-create-custom-order',
  templateUrl: './create-custom-order.component.html',
  styleUrls: ['./create-custom-order.component.css']
})
export class CreateCustomOrderComponent implements OnInit {
  id;
  orderDetails;
  customerDetail;
  channelDetails;
  customOrderForm: FormGroup;
  customOrder: CustomOrder;
  customProduct: CustomOrder;
  shiprockerOrder: ShiprocketOrder;
  shiprocketOrderDetails;
  pickupAddress;
  selectedPickupLocation;
  orderItems;
  productItems
  constructor(private shiprocketService: ShiprocketService, private route: ActivatedRoute, private fb: FormBuilder, private router: Router) {
    this.id = this.route.snapshot.params.id
  }

  ngOnInit() {
    this.getOrderDetails();
    this.createForm();
    this.getPickUpAddress();
  }
  createForm() {
    this.customOrderForm = this.fb.group({
      buyerPhoneNumber: [''],
      buyerName: [''],
      buyerEmailId: [''],
      buyerAddressLine1: [''],
      buyerAddresLine2: [''],
      pincode: [''],
      city: [''],
      state: [''],
      country: [''],
      orderId: [''],
      orderDate: [''],
      orderChannel: [''],
      productDetails: this.fb.array([]),
      payments: [''],
      pickupAddress: [''],
      weight: ['', Validators.required],
      height: ['', Validators.required],
      width: ['', Validators.required],
      length: ['', Validators.required],
      subTotal: [''],
      discount: [''],
      shippingCharges: [''],
      giftwrapCharges: [''],
      transactionCharges: [''],

    });
  }



  getOrderDetails() {
    this.shiprocketService.getSingleOrderDetails(this.id).subscribe(data => {
      this.orderDetails = data;
      this.getCustomerDetail(data.customerId);
      this.getChannelDetails();
      this.orderedProducts();
    }, err => {
      console.log(err);
    })
  }
  getCustomerDetail(id) {
    this.shiprocketService.getSingleCustomer(id).subscribe(data => {
      this.customerDetail = data;
    }, err => {
      console.log(err);
    });
  }

  getChannelDetails() {
    this.shiprocketService.getChannels().subscribe(data => {
      this.channelDetails = data;
    }, err => {
      console.log(err);
    })
  }

  getPickUpAddress() {
    this.shiprocketService.getPickUpAddress().subscribe(data => {
      this.pickupAddress = data.data.shipping_address;
    }, err => {
      console.log(err);
    })
  }
  selectedPickUpAddress(pickupLocation) {
    this.selectedPickupLocation = pickupLocation;
  }
  orderedProducts() {
    this.customProduct = new CustomOrder();
    this.customProduct.order_items = new Array();
    this.orderDetails.orderedProducts.forEach(element => {
      const filterData = element.child.filter(o => this.orderDetails.cart.find(o2 =>
        o.INTsku === o2.INTsku
      ));
      const filteredQty = this.orderDetails.cart.filter(o => filterData.find(o2 =>
        o.INTsku === o2.INTsku));
      this.customProduct.order_items.push({
        'name': filterData[0].productName,
        'sku': filterData[0].sku,
        'hsn': filterData[0].hsnCode,
        'selling_price': filterData[0].sp.toString(),
        'units': filteredQty[0].qty
      });
      console.log('product', this.customProduct);
    });


  }
  customOrderCreation() {
    this.customOrder = new CustomOrder();
    this.customOrder.order_id = this.orderDetails.orderId;
    this.customOrder.order_date = this.orderDetails.orderDate;
    this.customOrder.channel_id = this.channelDetails.data[0].id;
    this.customOrder.billing_customer_name = this.orderDetails.addressDetails[0].name;
    this.customOrder.billing_last_name = this.orderDetails.addressDetails[0].lastName;
    this.customOrder.billing_phone = this.orderDetails.addressDetails[0].mobileNumber.toString();
    this.customOrder.billing_address = this.orderDetails.addressDetails[0].building;
    this.customOrder.billing_address_2 = this.orderDetails.addressDetails[0].streetAddress;
    this.customOrder.billing_email = this.customerDetail.emailId;
    this.customOrder.billing_city = this.orderDetails.addressDetails[0].city;
    this.customOrder.billing_pincode = this.orderDetails.addressDetails[0].pincode;
    this.customOrder.billing_state = this.orderDetails.addressDetails[0].state;
    this.customOrder.billing_country = this.orderDetails.addressDetails[0].country;
    /* this.customOrder.payment_method = "Prepaid"; */
    this.customOrder.payment_method = this.orderDetails.paymentMode === 'COD' ? 'COD' : 'Prepaid';
    this.customOrder.pickup_location = this.selectedPickupLocation;
    this.customOrder.weight = this.customOrderForm.controls.weight.value;
    this.customOrder.height = this.customOrderForm.controls.height.value;
    this.customOrder.length = this.customOrderForm.controls.length.value;
    this.customOrder.breadth = this.customOrderForm.controls.width.value;
    this.customOrder.sub_total = this.orderDetails.total;
    this.customOrder.order_items = this.customProduct.order_items;
    this.customOrder.shipping_is_billing = true;
    this.shiprocketService.createCustomOrder(this.customOrder).subscribe(data => {
      console.log('order creation', data)
     this.shiprocketOrderDetails = JSON.parse(data);
      this.shiprockerOrder = new ShiprocketOrder();
      this.shiprockerOrder.order_id = this.shiprocketOrderDetails.order_id;
      this.shiprockerOrder.shipment_id = this.shiprocketOrderDetails.shipment_id;
      this.shiprockerOrder.status = this.shiprocketOrderDetails.status;
      this.shiprockerOrder.status_code = this.shiprocketOrderDetails.status_code;
      this.shiprockerOrder.onboarding_completed_now = this.shiprocketOrderDetails.onboarding_completed_now;
      this.shiprockerOrder.websiteOrderId = this.orderDetails.orderId;
      this.shiprockerOrder.orderObjectId = this.id;
      this.createShiprocketOrder(this.shiprockerOrder);
    }, err => {
      console.log(err);
    })

  }
  createShiprocketOrder(shiprocketOrderDetails) {
    this.shiprocketService.createShiprocketOrder(shiprocketOrderDetails).subscribe(data => {
      console.log(data);
      this.router.navigate(['/shiprocket/shiprocketorders'])
      console.log('success')
    }, err => {
      console.log(err);
    })
  }
}
