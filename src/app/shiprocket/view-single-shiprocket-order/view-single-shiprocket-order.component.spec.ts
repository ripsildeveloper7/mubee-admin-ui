import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSingleShiprocketOrderComponent } from './view-single-shiprocket-order.component';

describe('ViewSingleShiprocketOrderComponent', () => {
  let component: ViewSingleShiprocketOrderComponent;
  let fixture: ComponentFixture<ViewSingleShiprocketOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewSingleShiprocketOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSingleShiprocketOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
