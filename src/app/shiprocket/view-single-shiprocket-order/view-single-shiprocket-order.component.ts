import { Component, OnInit } from '@angular/core';
import { ShiprocketService } from '../shiprocket.service';
import { ActivatedRoute, Route, Router } from '@angular/router';

@Component({
  selector: 'app-view-single-shiprocket-order',
  templateUrl: './view-single-shiprocket-order.component.html',
  styleUrls: ['./view-single-shiprocket-order.component.css']
})
export class ViewSingleShiprocketOrderComponent implements OnInit {
  id;
  shiprocketOrder;
  constructor(private shiprocketService: ShiprocketService, private router: Router, private route: ActivatedRoute) {
    this.id = this.route.snapshot.params.orderid;
  }

  ngOnInit() {
    this.getSingleOrderDetails();
  }
  getSingleOrderDetails() {
    this.shiprocketService.getSingleOrder(this.id).subscribe(data => {
      this.shiprocketOrder = data.data;
      console.log(data);
    }, err => {
      console.log(err);
    })
  }
  printLabel(id) {
    this.shiprocketService.printLabel(id).subscribe(data => {
      console.log(data, 'print label');
    }, err => {
      console.log(err);
    })
  }
  requestPickUp(id) {
    console.log('id', id)
    this.shiprocketService.requestPickup(id).subscribe(data => {
      console.log(data, 'request pickup');
    }, err => {
      console.log(err);
    })
  }
}
