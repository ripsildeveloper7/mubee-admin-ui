import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GetChannelComponent} from './get-channel/get-channel.component';
import {ViewPickupAddressComponent} from './view-pickup-address/view-pickup-address.component';
import {CreateCustomOrderComponent} from './create-custom-order/create-custom-order.component';
import {ViewShiprokcetOrdersComponent} from './view-shiprokcet-orders/view-shiprokcet-orders.component';
import {CheckCourierServiceabilityComponent} from './check-courier-serviceability/check-courier-serviceability.component';
import {ViewSingleShiprocketOrderComponent} from './view-single-shiprocket-order/view-single-shiprocket-order.component';
import {ViewOrderTrackingDetailsComponent} from './view-order-tracking-details/view-order-tracking-details.component';
const routes: Routes = [{
  path: 'getchannel',
  component: GetChannelComponent
},
{
  path: 'pickupaddress',
  component: ViewPickupAddressComponent
},
{
  path: 'customorder/:id',
  component: CreateCustomOrderComponent
},
{
  path: 'shiprocketorders',
  component: ViewShiprokcetOrdersComponent
},
{
  path: 'courierservice/:orderid',
  component: CheckCourierServiceabilityComponent
},
{
  path: 'singleorder/:orderid',
  component: ViewSingleShiprocketOrderComponent
},
{
  path: 'trackorder/:orderid',
  component: ViewOrderTrackingDetailsComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShiprocketRoutingModule { }
