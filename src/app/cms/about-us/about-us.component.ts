import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AboutUs } from './aboutus.model';
import { CmsService } from '../cms.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {
  AboutUsForm: FormGroup;
  aboutModel: AboutUs;
  message: string;
  action: any;
  fileToUpload: any;
  reader: FileReader;
  urls: any;
  fileLength: any;

  constructor(private fb: FormBuilder, private cmsService: CmsService, private router: Router,
              private snackBar: MatSnackBar) { }
  ngOnInit() {
    this.createForm();
  }
  createForm() {
    this.AboutUsForm = this.fb.group({
      title: [''],
      details: [''],
      note1: [''],
      note2: [''],
      note3: ['']
    });
  }
  cancel() {
    this.router.navigate(['cms/viewaboutus']);
  }
  createAboutUs(AboutUsForm: FormGroup) {
    this.message = 'About Us Created Successfully';                     // Create Terms and Use
    this.aboutModel = new AboutUs();
    this.aboutModel.Title = AboutUsForm.controls.title.value;
    this.aboutModel.Details = AboutUsForm.controls.details.value;
    this.aboutModel.note1 = AboutUsForm.controls.note1.value;
    this.aboutModel.note2 = AboutUsForm.controls.note2.value;
    this.aboutModel.note3 = AboutUsForm.controls.note3.value;
    this.cmsService.createAboutUs(this.aboutModel).subscribe(data => {
      this.aboutModel = data;
      console.log(this.aboutModel,"about")
      this.addImage(data._id);
      this.snackBar.open(this.message, this.action, {
        duration: 2000,
      });
      this.router.navigate(['cms/viewaboutus']);
    }, error => {
      console.log(error);
    });
  }

  handleFileInput(images: any) {
    this.fileToUpload = images;
    this.aboutModel.ImageName= this.fileToUpload[0];
    this.urls = [];
    const files = images;
    if (files) {
      for (const file of files) {
        this.reader = new FileReader();
        this.reader.onload = (e: any) => {
          this.urls.push(e.target.result);
        };
        this.reader.readAsDataURL(file);
      }
    }
    
  }

  addImage(id) {                                     // Upload Brand Images
    this.message = 'Image added';
    const formData: any = new FormData();
    this.fileLength = this.fileToUpload.length;
    for (let i = 0; i <= this.fileLength; i++) {
      formData.append('single', this.fileToUpload[i]);
    }
    this.cmsService.uploadImage(formData, id).subscribe(data => {
      this.updateImageName(data, id);
      this.snackBar.open(this.message, this.action, {
        duration: 2000,
      });
    }, error => {
      console.log(error);
    });
  }

  updateImageName(data, id) {                                     // Upload Brand Images
   
    this.aboutModel.ImageName = data.originalname;
    this.cmsService.editAboutUs(this.aboutModel).subscribe(data1 => {
      this.snackBar.open(this.message, this.action, {
        duration: 2000,
      });
      this.router.navigate(['brand/viewbrand']);
    }, error => {
      console.log(error);
    });
  }

}
