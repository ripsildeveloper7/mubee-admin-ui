import { Component, OnInit } from '@angular/core';
import { CommunityService } from '../../service/community.service';

@Component({
  selector: 'app-view-community',
  templateUrl: './view-community.component.html',
  styleUrls: ['./view-community.component.css']
})
export class ViewCommunityComponent implements OnInit {
  communityModel;
  constructor(public communityService: CommunityService ) { }

  ngOnInit() {
  }
  getCommunityuser() {
    this.communityService.getCommunityUser().subscribe(data => {
      this.communityModel = data;
      }, error => {
          console.log(error);
      });
  }
  
}
