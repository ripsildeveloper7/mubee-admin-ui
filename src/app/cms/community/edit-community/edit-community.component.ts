import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CommunityService } from '../../service/community.service';
import { Router } from '@angular/router';
import { Community } from 'src/app/shared/model/community.model';

@Component({
  selector: 'app-edit-community',
  templateUrl: './edit-community.component.html',
  styleUrls: ['./edit-community.component.css']
})
export class EditCommunityComponent implements OnInit {
  communityDisplay: Community;
  communityFieldForm: FormGroup;
  communityModel: Community[];
  community: Community;
  fileLength;
  fileToUpload;
  showEdit: boolean;
  urls = new Array<string>();
  reader: FileReader = new FileReader();
  BASE64_MARKER: any = ';base64,';
  check;
  campaignForm: FormGroup;
  showImage: any;
  brandId: string;
  submitted: boolean = false;
  constructor(private fb: FormBuilder, public communityService: CommunityService, private router: Router ) { }

  ngOnInit() {
    this.getCommunity()
  }

  getCommunity() {
    this.communityService.getCommunity().subscribe(data => {
      this.communityDisplay = data;
      
     /*  if(this.communityModel.length === 0){
        
        this.showEdit = true;
      } else {
        this.showEdit = false;
      } */
      
      }, error => {
          console.log(error);
      });
  }
  
handleFileInput64Base(files: FileList) {
  this.fileToUpload = files[0];
  this.community = new Community();
  const reader = new FileReader();
  const file = this.fileToUpload;
  reader.readAsDataURL(file);
  reader.onload = () => {
    this.check = reader.result;
    this.community.bannerImageName = this.fileToUpload.name;
    this.community.fileName = this.check;
    this.showImage = this.check;
  };
}
addCommunityImage(communityId){
  this.communityService.editAndUpdateImage(communityId,  this.community).subscribe(data => {
    this.communityDisplay = data;
   }, error => {
       console.log(error);
   });
}
editHeading(){
  this.communityDisplay.showHeadingDiv = true;
}
editSubHeading(){
  this.communityDisplay.showSubHeadingDiv = true;
}
updateHeading(communityId, heading){
  this.community = new Community();
  this.community.heading = heading;
  this.communityService.editHeading(communityId,  this.community).subscribe(data => {
    this.communityDisplay = data;
    
   }, error => {
       console.log(error);
   });
}
updateSubHeading(communityId, subHeading){
  this.community = new Community();
  this.community.subHeading = subHeading;
  this.communityService.editSubHeading(communityId,  this.community).subscribe(data => {
    this.communityDisplay = data;
   }, error => {
       console.log(error);
   });
}
updateCommunityDescription(communityId, descriptionId, description) {
  this.community = new Community();
  this.community.description = description;
  this.communityService.editDescription(communityId, descriptionId,  this.community).subscribe(data => {
    this.communityDisplay = data;
   }, error => {
       console.log(error);
   });
}
editCommunityDescription(desId){
  this.communityDisplay.communityDescription.forEach(b => {
    b.showDescription = false;
    if(b._id === desId) {
      b.showDescription = true;
    }
  } );
}
editCommunity(bannerId){
  
      this.communityDisplay.bannerImage.forEach(b => {
          b.showDiv = false;
          if(b._id === bannerId) {
            b.showDiv = true;
          }
        } );
}

updateCommunityImage(communityId, bannerId, imageName) {
  this.communityService.editBannerImage(communityId, bannerId, imageName, this.community).subscribe(data => {
     this.communityDisplay = data;
     
    }, error => {
        console.log(error);
    });
}
}
