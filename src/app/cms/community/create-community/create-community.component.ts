import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import {MatPaginator,MatDialog, MatTableDataSource} from '@angular/material';
import {MatIconModule} from '@angular/material/icon';
import { Community   } from './../../../shared/model/community.model';
import { CommunityService } from './../../service/community.service';

@Component({
  selector: 'app-create-community',
  templateUrl: './create-community.component.html',
  styleUrls: ['./create-community.component.css']
})
export class CreateCommunityComponent implements OnInit {

  constructor(private fb: FormBuilder, private communityService: CommunityService, private router: Router ) { }
  communityFieldForm: FormGroup;
  communityModel: Community;
  community: Community;
  fileLength;
  fileToUpload;
  showEdit: boolean;
  urls = new Array<string>();
  reader: FileReader = new FileReader();
  BASE64_MARKER: any = ';base64,';
  check;
  campaignForm: FormGroup;
  showImage: any;
  brandId: string;
  submitted: boolean = false;
  ngOnInit() {
    
    this.getCommunity();
    this.createForm();
  }
  createForm() {
    this.communityFieldForm = this.fb.group({
      bannerImageName: ['', Validators.required],
      heading: ['', Validators.required],
      subHeading: ['', Validators.required],
      fileName: [''],
      communityDescription: this.fb.array([])
    });
  }
  
  addForm() {
    const fieldValue = this.fb.group({
      description: ['', Validators.required]
    });
    this.FieldsValueForms.push(fieldValue);
  }
  get FieldsValueForms() {
    return this.communityFieldForm.get('communityDescription') as FormArray;
  }
  deleteInvoiceParticulars(idx: number) {
    this.FieldsValueForms.removeAt(idx);
    
  }
  addCommunityPage() {
      this.community = new Community();
      this.community.heading = this.communityFieldForm.controls.heading.value,
      this.community.subHeading = this.communityFieldForm.controls.subHeading.value,
      this.community.bannerImageName = this.communityFieldForm.controls.bannerImageName.value,
      this.community.fileName = this.communityFieldForm.controls.fileName.value,
      this.community.communityDescription = this.communityFieldForm.controls.communityDescription.value;
      this.communityService.addCommunityPageContent(this.community).subscribe(data => {
        this.updateCampaignImage(data._id)
        this.community = data;
    });
}

getDeleteSingleSuper(value) {
 /*  this.categoryService.deleteSuperCategory(value).subscribe(deleteData => {
    this.superCategoryModel = deleteData;
  },error => {
    console.log(error);
  }); */
}
getEditSingle(id){
/*   this.superCategoryModel.forEach(element => {
      if (element._id === id ) {
        element.editCat = true;
      } else {
        element.editCat = false;
      }
    }); */
}
/* cancerSuperCat(id){
  this.superCategoryModel.forEach(element => {
    if (element._id === id ) {
      element.editCat = false;
    }
  });
}
updateCategory(id, categoryName, categoryDescription, sortOrder){
  this.superCategory = new SuperCategory();
  this.superCategory.categoryName = categoryName.value;
  this.superCategory.categoryDescription = categoryDescription.value;
  this.superCategory.sortOrder = sortOrder.value;
  this.categoryService.updateSuperCategory(this.superCategory, id).subscribe(data => {
    this.superCategoryModel = data;
  }, error => {
    console.log(error);
  });
}
getSuperCategory() {
  this.categoryService.getSuperCategory().subscribe(data => {
    this.superCategoryModel = data;
  });
} */
handleFileInput64Base(files: FileList) {
  this.fileToUpload = files[0];
  this.community = new Community();
  const reader = new FileReader();
  const file = this.fileToUpload;
  reader.readAsDataURL(file);
  reader.onload = () => {
    this.check = reader.result;
    this.community.bannerImage = this.fileToUpload.name;
    this.community.fileName = this.check;
    this.showImage = this.check;
    this.communityFieldForm.controls.fileName.setValue(this.showImage);
    this.communityFieldForm.controls.bannerImageName.setValue(this.fileToUpload.name);
  };
}


updateCampaignImage(communityId) {
  this.communityService.editAndUpdateImage(communityId, this.community).subscribe(data => {
    /* this.community = data; */
    this.router.navigate(['cms/edit-community'])
    }, error => {
        console.log(error);
    });
}


getCommunity() {
  this.communityService.getCommunity().subscribe(data => {
    this.communityModel = data;
    if(!this.communityModel){
    
    
   
    } else {
      this.router.navigate(['cms/edit-community'])
    }
    
    }, error => {
        console.log(error);
    });
}
}