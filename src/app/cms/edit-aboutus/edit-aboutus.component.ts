import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AboutUs } from '../about-us/aboutus.model';
import { CmsService } from '../cms.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-edit-aboutus',
  templateUrl: './edit-aboutus.component.html',
  styleUrls: ['./edit-aboutus.component.css']
})
export class EditAboutusComponent implements OnInit {

  AboutUsForm: FormGroup;
  aboutModel: AboutUs;
  message: string;
  action: any;
  id: string;

  constructor(private cmsService: CmsService, private fb: FormBuilder, private router: Router,
              private route: ActivatedRoute) {
                this.route.paramMap.subscribe((params: ParamMap) => {
                  this.id = params.get('id');
                });
              }

  ngOnInit() {
    this.createForm();
    this.getAboutUs();
  }
  createForm() {
    this.AboutUsForm = this.fb.group({
      title: [''],
      details: [''],
      note1: [''],
      note2: [''],
      note3: ['']
    });
  }
  getAboutUs() {
    this.cmsService.getSingleAboutUs(this.id).subscribe(data => {
      this.aboutModel = data;
      console.log(data);
    }, error => {
      console.log(error);
    });
  }
  updateTermsAndUse(TermsForm: FormGroup) {
    this.aboutModel = new AboutUs();
    this.aboutModel.Title = this.AboutUsForm.controls.title.value;
    this.aboutModel.Details = this.AboutUsForm.controls.details.value;
    this.aboutModel.note1 = this.AboutUsForm.controls.note1.value;
    this.aboutModel.note2 = this.AboutUsForm.controls.note2.value;
    this.aboutModel.note3 = this.AboutUsForm.controls.note3.value;
    this.cmsService.UpdateTermsAndUse(this.aboutModel, this.id).subscribe(data => {
      this.router.navigate(['cms/viewaboutus']);
    }, error => {
      console.log(error);
    });
  }
  cancel() {
    this.router.navigate(['cms/viewaboutus']);
  }
}
