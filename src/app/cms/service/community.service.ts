import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppSetting } from '../../config/appSetting';
import { Community } from './../../shared/model/community.model';

@Injectable({
  providedIn: 'root'
})
export class CommunityService {
  
  contentServiceUrl: string = AppSetting.contentServiceUrl;
  contentImageUrl: string = AppSetting.contentImageUrl;
  constructor(private httpClient: HttpClient) { }
  addCommunityPageContent(community: Community): Observable<Community> {
    const customUrl = 'createcommunitypage';
    const url: string = this.contentServiceUrl + customUrl;
    return this.httpClient.post<Community>(url, community);
  }
  editAndUpdateImage(communityId, community): Observable<Community>{
    const customUrl = 'createbannerimage';
    const url: string = this.contentServiceUrl + customUrl + '/'+ communityId ;
    return this.httpClient.put<Community>(url, community);
  }
  editHeading(communityId, community): Observable<Community>{
    const customUrl = 'editcommunitypageheader';
    const url: string = this.contentServiceUrl + customUrl + '/'+ communityId ;
    return this.httpClient.put<Community>(url, community);
  }
  editSubHeading(communityId, community): Observable<Community>{
    const customUrl = 'editcommunitypagesubheader';
    const url: string = this.contentServiceUrl + customUrl + '/'+ communityId ;
    return this.httpClient.put<Community>(url, community);
  }
  editDescription(communityId, descriptionId, community): Observable<Community>{
    const customUrl = 'editcommunitypagedescription';
    const url: string = this.contentServiceUrl + customUrl + '/'+ communityId + '/' + descriptionId;
    return this.httpClient.put<Community>(url, community);
  }
  editBannerImage(communityId, bannerId, imageName, community): Observable<Community>{
    const customUrl = 'createbannerimage';
    const url: string = this.contentServiceUrl + customUrl + '/'+ communityId  + '/' + bannerId +'/'+ imageName;
    return this.httpClient.put<Community>(url, community);
  }
  getCommunity(): Observable<Community>{
    const customUrl = 'getallcommunity';
    const url: string = this.contentServiceUrl + customUrl;
    return this.httpClient.get<Community>(url);
  }
  getCommunityUser(): Observable<Community> {
    const customUrl = 'getallcommunityuser';
    const url: string = this.contentServiceUrl + customUrl;
    return this.httpClient.get<Community>(url);
  }
}
