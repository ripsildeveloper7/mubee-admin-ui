import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePromotionThreeBannerComponent } from './create-promotion-three-banner.component';

describe('CreatePromotionThreeBannerComponent', () => {
  let component: CreatePromotionThreeBannerComponent;
  let fixture: ComponentFixture<CreatePromotionThreeBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePromotionThreeBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePromotionThreeBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
