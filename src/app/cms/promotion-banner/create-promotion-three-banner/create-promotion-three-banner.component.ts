import { Component, OnInit } from '@angular/core';
import { CmsService } from '../../cms.service';
import { Router } from '@angular/router';
import { PromotionBannerImage } from '../create-promotion-one-banner/promotionBanner.model';

@Component({
  selector: 'app-create-promotion-three-banner',
  templateUrl: './create-promotion-three-banner.component.html',
  styleUrls: ['./create-promotion-three-banner.component.css']
})
export class CreatePromotionThreeBannerComponent implements OnInit {
  fileToUpload: File;
  bannerModel;
  check: string | ArrayBuffer;
  showImage: string | ArrayBuffer;
  urls: any[];

  constructor(private cmsService: CmsService, private router: Router) { }

  ngOnInit() {
  }
  handleFileInput(files: FileList) {
    this.fileToUpload = files[0];
    this.bannerModel = new PromotionBannerImage();
    const reader = new FileReader();
    this.urls = [];
    const file = this.fileToUpload;
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.urls.push(reader.result);
      this.check = reader.result;
      this.bannerModel.imageName = this.fileToUpload.name;
      this.bannerModel.base64Image = this.check;
      this.showImage = this.check;
    };
  }
  onSubmit(tagN, tagD, link) {
    const obj: any = {};
    obj.tagTitle = tagN;
    obj.tagDescription = tagD;
    obj.link = link;
    obj.position = 3;
    this.cmsService.createPromotionBanner(obj).subscribe(data => {
      this.updatePromotionBannerImage(data._id);
    }, error => {
      console.log(error);
    });
  }
  updatePromotionBannerImage(id) {
    this.cmsService.uploadPromotionBannerBase64(this.bannerModel, id).subscribe(data => {
      this.updatePromotionBannerImageName(id);
    }, error => {
      console.log(error);
    });
  }
  updatePromotionBannerImageName(id) {
    this.cmsService.updatePromotionBannerName(this.bannerModel, id).subscribe(data => {
      this.router.navigate(['cms/promotionThreeBanner']);
    }, error => {
      console.log(error);
    });
  }
}