import { Component, OnInit } from '@angular/core';
import { CmsService } from '../../cms.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-view-promotion-three-banner',
  templateUrl: './view-promotion-three-banner.component.html',
  styleUrls: ['./view-promotion-three-banner.component.css']
})
export class ViewPromotionThreeBannerComponent implements OnInit {
  holder: any;
  showAddButton = false;
  constructor(private cmsService: CmsService, private router: Router) { 
    this.getPromotionOne();
  }
  ngOnInit() {
  }
  getPromotionOne() {
    this.showAddButton = false;
    const obj: any = {};
    obj.position = 3;
    this.cmsService.getSinglePromotionBannerByPosition(obj).subscribe(data => {
      console.log('bn', data);
      if (data.length === 0) {
        this.showAddButton = true;
      } else {
        this.holder = data[0];
      }
    }, error => {
      console.log(error);
    });
  }
onAdd() {
  this.router.navigate(['cms/addPromotionBannerThree']);
}
changeStatus(status, id) {
  const obj: any = {};
  obj.status = status === 'Enabled' ? 'Disabled' : 'Enabled';
  obj.position = 3;
  console.log(obj, id);
  this.cmsService.updatePromotionBannerStatus(obj, id).subscribe(data => {
    this.holder = data;
  }, error => {
    console.log(error);
  });
}
onEdit(id) {
  this.router.navigate(['cms/editPromotionBanner/', id]);
}
}

