import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPromotionThreeBannerComponent } from './view-promotion-three-banner.component';

describe('ViewPromotionThreeBannerComponent', () => {
  let component: ViewPromotionThreeBannerComponent;
  let fixture: ComponentFixture<ViewPromotionThreeBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPromotionThreeBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPromotionThreeBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
