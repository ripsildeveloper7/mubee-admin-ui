import { Component, OnInit } from '@angular/core';
import { CmsService } from '../../cms.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { PromotionBannerImage } from '../create-promotion-one-banner/promotionBanner.model';

@Component({
  selector: 'app-edit-promotion-banner',
  templateUrl: './edit-promotion-banner.component.html',
  styleUrls: ['./edit-promotion-banner.component.css']
})
export class EditPromotionBannerComponent implements OnInit {
  id: string;
  holder: any;
  isShowPromotion = false;
  fileToUpload: File;
  bannerModel;
  urls: any[];
  check: string | ArrayBuffer;
  showImage: string | ArrayBuffer;
  hide = false;

  constructor(private cmsService: CmsService, private router: Router, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get('id');
    });
    this.getPromotionBanner();
   }

  ngOnInit() {
  }
  getPromotionBanner() {
    this.cmsService.getSinglePromotionBanner(this.id).subscribe(data => {
      this.holder = data;
    }, error => {
      console.log(error);
    });
  }
  onImageChange() {
    this.isShowPromotion = true;
  }
  onCancelImage() {
    this.isShowPromotion = false;
    this.urls = [];
    this.hide = false;
  }
  handleFileInput(files: FileList) {
    this.fileToUpload = files[0];
    this.bannerModel = new PromotionBannerImage();
    const reader = new FileReader();
    this.urls = [];
    const file = this.fileToUpload;
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.urls.push(reader.result);
      this.check = reader.result;
      this.bannerModel.imageName = this.fileToUpload.name;
      this.bannerModel.base64Image = this.check;
      this.showImage = this.check;
    };
  }
  onUpdate(tagN, tagD, link) {
    if (this.isShowPromotion) {
      if (this.bannerModel !== undefined) {
        this.updatePromotionBannerImage(tagN, tagD, link);
      } else {
        this.hide = true;
      }
    } else {
      this.onSubmit(tagN, tagD, link);
    }
  }
  updatePromotionBannerImage(tagN, tagD, link) {
    this.cmsService.uploadPromotionBannerBase64(this.bannerModel, this.id).subscribe(data => {
      this.onSubmit(tagN, tagD, link);
    }, error => {
      console.log(error);
    });
  }
  onSubmit(tagN, tagD, link) {
    const obj: any = {};
    obj.tagTitle = tagN;
    obj.tagDescription = tagD;
    obj.link = link;
    obj.imageName = this.isShowPromotion === true ? this.bannerModel.imageName : this.holder.imageName;
    this.cmsService.updatePromotionBanner(obj, this.id).subscribe(data => {
      if (this.holder.position === 1) {
        this.router.navigate(['cms/promotionOneBanner']);
      } else {
        this.router.navigate(['cms/promotionSecondBanner']);
      }
    }, error => {
      console.log(error);
    });
  }
}
