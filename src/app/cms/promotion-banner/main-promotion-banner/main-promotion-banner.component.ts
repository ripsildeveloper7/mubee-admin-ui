import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-promotion-banner',
  templateUrl: './main-promotion-banner.component.html',
  styleUrls: ['./main-promotion-banner.component.css']
})
export class MainPromotionBannerComponent implements OnInit {
  promotionTab = [ {name: 'Promotion 1' , link: '/cms/promotionOneBanner'},
  {name: 'Promotion 2' , link: '/cms/promotionSecondBanner'},
  {name: 'Promotion 3' , link: '/cms/promotionThreeBanner'},

];
  selectedItemTab = this.promotionTab[0].name;
  constructor() { }

  ngOnInit() {
  }
  selectedTab(tab) {
    this.selectedItemTab = tab;
  }
}
