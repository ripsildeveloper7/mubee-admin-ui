import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { CmsService } from '../cms.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { DeleteConfirmBoxComponent } from 'src/app/shared/delete-confirm-box/delete-confirm-box.component';

@Component({
  selector: 'app-view-about-us',
  templateUrl: './view-about-us.component.html',
  styleUrls: ['./view-about-us.component.css']
})
export class ViewAboutUsComponent implements OnInit {
  displayedColumns: string[] = ['heading', 'details', 'image', 'Notes' ,'Action'];
  aboutModel: any;
  message: string;
  action: any;
  showNoData:boolean;
  constructor(private fb: FormBuilder, private cmsService: CmsService,public dialog: MatDialog, private router: Router,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getAboutUs();
  }
  getAboutUs() {                                              // Retrieve All Terms and use
    this.cmsService.getAboutUs().subscribe(data => {
      this.aboutModel = data;
      console.log(this.aboutModel)
      if(data.length === 0) {
        this.showNoData = true;
      } else {
        this.showNoData = false;
      }
    }, error => {
      console.log(error);
    });
  }
  deleteAboutUs(data) {
    this.message = 'Deleted Successfully';                                    // Delete Single Terms and use
    this.cmsService.deleteAboutUs(data._id).subscribe(value => {
      this.aboutModel = value;
      this.snackBar.open(this.message, this.action, {
        duration: 2000,
      });
    }, error => {
      console.log(error);
    });
  }
  openDialog(data):void {
    const dialogRef = this.dialog.open(DeleteConfirmBoxComponent, {
      width: '350px',
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
       
        this.deleteAboutUs(data);
      }
    });
  }
  editAboutUs(data) {
    this.router.navigate(['cms/editaboutus', data._id]);
  }
  goToAdd() {
    this.router.navigate(['cms/aboutus']);
  }
}
