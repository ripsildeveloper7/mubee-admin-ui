
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import {MatPaginator,MatDialog, MatTableDataSource} from '@angular/material';
import {MatIconModule} from '@angular/material/icon';
import { ReferAndEarn   } from './../../../shared/model/referAndEarn.model';
import { MarketingService } from './../../marketing.service';

@Component({
  selector: 'app-add-refer-earn',
  templateUrl: './add-refer-earn.component.html',
  styleUrls: ['./add-refer-earn.component.css']
})
export class AddReferEarnComponent implements OnInit {
  referAndEarnFieldForm: FormGroup;
  referAndEarnModel: ReferAndEarn[];
  referAndEarn: ReferAndEarn;
  errorEmailMessage= false;
  submitted = false;
  constructor(public marketingService: MarketingService, private fb: FormBuilder) { }

  ngOnInit() {
    this.createForm();
    this.getReferAndEarn();
  }

  createForm() {
    this.referAndEarnFieldForm = this.fb.group({
      couponName:  ['', Validators.required],
      couponDescription:  ['', Validators.required],
      referrerPercentage: ['', [Validators.required, Validators.pattern('[0-9]\\d{1}')]],
      applierPercetage: ['', [Validators.required, Validators.pattern('[0-9]\\d{1}')]],
      countOfApplier: ['', [Validators.required]],
      startDate: ['' , Validators.required],
      endDate: ['' , Validators.required],

    }, {validator: this.checkDates});
  }
  get f() { 
      
    return this.referAndEarnFieldForm.controls;
  }
  checkDates(group: FormGroup) {
    if(group.controls.endDate.value) {
    if(group.controls.endDate.value <= group.controls.startDate.value) {
      return { notValid:true }
    }
    return null;
  }
  }
  addReferAndEarn() {
    this.submitted = true;
    if (this.referAndEarnFieldForm.invalid) {
      this.referAndEarnFieldForm.markAllAsTouched()
      return;
  } else {
    
    this.referAndEarn = new ReferAndEarn();
    this.referAndEarn.couponName = this.referAndEarnFieldForm.controls.couponName.value,
    this.referAndEarn.couponDescription = this.referAndEarnFieldForm.controls.couponDescription.value,
    this.referAndEarn.referrerPercentage = this.referAndEarnFieldForm.controls.referrerPercentage.value,
    this.referAndEarn.applierPercetage = this.referAndEarnFieldForm.controls.applierPercetage.value,
    this.referAndEarn.countOfApplier = this.referAndEarnFieldForm.controls.countOfApplier.value,
    this.referAndEarn.startDate = this.referAndEarnFieldForm.controls.startDate.value,
    this.referAndEarn.endDate = this.referAndEarnFieldForm.controls.endDate.value,
    this.marketingService.addReferAndEarn(this.referAndEarn).subscribe(data => {
      this.referAndEarnModel = data;
  });
}}
_keyPress(event: any) {
  const pattern = /[0-9]/;
  let inputChar = String.fromCharCode(event.charCode);
  if (!pattern.test(inputChar)) {
      event.preventDefault();

  }
}
editCheckDates(startDate, endDate) {
  if(endDate) {
  if(endDate <= startDate) {
    return {

      notValid:true }
  }
  return false;
}
}


getReferAndEarn() {
  this.marketingService.getReferAndEarn().subscribe(data => {
    this.referAndEarnModel = data;

    }, error => {
        console.log(error);
    });
}

referEdit(_id){
  this.referAndEarnModel.forEach(el => { 
    if (el._id === _id)
     {
el.showDiv = true;
  } else {
    el.showDiv = false;
  }
});
}
cancelReferAndEarn() {
  this.referAndEarnModel.forEach(el => { 
    el.showDiv = false;
});
}
referDelete(id){
  this.marketingService.deleteReferAndEarn(id).subscribe(data => {
    
  }, error => {
      console.log(error);
  });
}
updateReferAndEarn(_id, couponName, couponDescription, referrerPercentage, countOfApplier, applierPercetage, startDate, endDate){
  if(!this.editCheckDates(startDate, endDate))
  {
  this.referAndEarn = new ReferAndEarn();
  this.referAndEarn.couponName = couponName;
  this.referAndEarn.couponDescription = couponDescription;
  this.referAndEarn.referrerPercentage = referrerPercentage;
  this.referAndEarn.countOfApplier = countOfApplier; 
  this.referAndEarn.applierPercetage = applierPercetage;
  this.referAndEarn.startDate = startDate;
  this.referAndEarn.endDate = endDate;
  this.marketingService.editReferAndEarn(_id, this.referAndEarn).subscribe(data => {
    this.referAndEarnModel = data;
  }, error => {
      console.log(error);
  });
} else {
  this.referAndEarnModel.forEach(el => { 
    if (el._id === _id)
     {
    el.dateError = true;
  } else {
    el.dateError = false;
  }
});
}
}
getCommunityuser() {
  this.marketingService.getReferAndEarn().subscribe(data => {
    
    }, error => {
        console.log(error);
    });
}
}
