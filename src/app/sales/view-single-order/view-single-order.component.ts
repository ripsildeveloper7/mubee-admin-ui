import { Component, OnInit } from '@angular/core';
import {SalesService} from '../sales.service';
import {Order} from '../orders/order.model';
import { Route, ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import {AppSetting} from '../../config/appSetting';
import {CustomerModel} from './customer.model';
import { TailoringDetailService } from '../view-tailoring-detail/tailoring-detail.service';

@Component({
  selector: 'app-view-single-order',
  templateUrl: './view-single-order.component.html',
  styleUrls: ['./view-single-order.component.css']
})
export class ViewSingleOrderComponent implements OnInit {
  id;
  orderModel: Order;
  orderForm: FormGroup;
  productModel: any;
  message;
  action;
  serviceUrl;
  customerId;
  customerDetail: CustomerModel;
  productImageUrl: string = AppSetting.productImageUrl;
  status = ['New', 'Processing', 'Ready To Ship','Out For Delivery', 'OnHold', 'Completed', 'Cancelled', 'Failed'];
  constructor(private router: Router, private salesService: SalesService, private route: ActivatedRoute,
              private snackBar: MatSnackBar, private fb: FormBuilder, private tailoringService: TailoringDetailService) {
    this.id = this.route.snapshot.params.id;
  }

  ngOnInit() {
    this.createForm();
    this.viewOrderDetails();
  }
  createForm() {
    this.orderForm = this.fb.group({
      orderedDate: [''],
      statusType: [''],
      AWBNo: ['']
    });
  }
  viewOrderDetails() {
    this.salesService.getOrderDetails(this.id).subscribe(data => {
      this.orderModel = data;
      this.customerId = data.customerId;
      this.getCustomerDetail( this.customerId);
      console.log('single order details', this.orderModel);
    }, err => {
      console.log(err);
    });
  }
  getCustomerDetail(id) {
    this.salesService.getSingleCustomer(id).subscribe(data => {
      this.customerDetail = data;
      console.log('customer details', data);
    });
  }
  updateStatus()  {
    this.message = 'Order Updated';
    this.orderModel = new Order();
    this.orderModel.orderStatus = this.orderForm.controls.statusType.value;
    this.salesService.updateStatus(this.id, this.orderModel).subscribe(data => {
      this.orderModel = data;
      this.snackBar.open(this.message, this.action, {
        duration: 3000,
      });
     /*  this.router.navigate(['orders/vieworders']); */
    }, err => {
      console.log(err);
    });
  }
  updateAWB() {
    this.message = 'Order Updated';
    this.orderModel = new Order();
    this.orderModel.orderStatus = this.orderForm.controls.statusType.value;
    this.orderModel.awbNo = this.orderForm.controls.AWBNo.value;
    this.salesService.updateAWBStatus(this.id, this.orderModel).subscribe(data => {
      this.orderModel = data;
      this.snackBar.open(this.message, this.action, {
        duration: 3000,
      });
     /*  this.router.navigate(['orders/vieworders']); */
    }, err => {
      console.log(err);
    });
  }

  shiprocketCustomOrder(id) {
    console.log(id)
    this.router.navigate(['shiprocket/customorder', id]);
  }
 
}
