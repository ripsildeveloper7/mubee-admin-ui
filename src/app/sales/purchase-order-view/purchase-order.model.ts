import { PurchaseOrderItem } from './purchase-orderItem.model';

export class PurchaseOrder {
    poNumber: string;
    poType: string;
    poDate: Date;
    poExpiryDate: Date;
    billTo: string;
    gstNo: string;
    contactNo: string;
    vendorName: string;
    vendorAddress: string;
    vendorEmailId: string;
    vendorCSTNo: string;
    vendorGStNo: string;
    vendorCurrencyType: string;
    vendorId: string;
    totalAmount: number;
    freight: number;
    tax: number;
    totalQuentity: number;
    totalCGSTAmount: number;
    totalSGSTAmount: number;
    totalIGSTAmount: number;
    totalNetAmount: number;
    specialInstruction: string;
    vendorItems: [ PurchaseOrderItem ];
    orderId: string;
}
