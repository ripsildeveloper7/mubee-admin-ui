export class PurchaseOrderItem {
    catalogName: string;
    vendorItemNo: string;
    unitPrice: number;
    size: string;
    quantity: number;
    discount: number;
    unitOfMeasure: string;
    discountPercentage: number;
    cgstRate: number;
    sgstRate: number;
    igstRate: number;
    cgstAmount: number;
    sgstAmount: number;
    igstAmount: number;
    netAmount: number;
    estimatedShipmentDate: Date;
}
