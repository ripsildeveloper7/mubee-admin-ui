import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../product.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-price-rate',
  templateUrl: './price-rate.component.html',
  styleUrls: ['./price-rate.component.css']
})
export class PriceRateComponent implements OnInit {
  holder: any;
  hold: {
    priceRate: number,
  };
  constructor(private router: Router, private productService: ProductService) { 
    this.getPriceRate();
  }

  ngOnInit() {
  }
  getPriceRate() {
    this.productService.getPriceRate().subscribe(data => {
      this.holder = data[0];
    }, error => {
      console.log(error);
    });
  }
  addRate(rate) {
    this.hold = {
      priceRate: rate
    };
    this.productService.addPriceRate(this.hold).subscribe(data => {
      this.holder = data;
    }, error => {
      console.log(error);
    });
  }
}
