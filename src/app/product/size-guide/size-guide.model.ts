
export class SizeGuide {
  sizeGuideName: string;
  superCategoryId: string;
    mainCategoryId: string;
    subCategoryId: string;
    superCategoryName: string;
    mainCategoryName: string;
    subCategoryName: string;
    sizeChartCM: string;
    sizeChartInches: string;
    brandName:string;
    brandId:string;
}
