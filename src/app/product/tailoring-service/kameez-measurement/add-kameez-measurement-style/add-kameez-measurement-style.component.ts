import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { ProductService } from '../../../product.service';
import { MeasurementImage } from '../create-kameez-measurement/measurementImage.model';
import { KameezMeasure } from '../../../../shared/model/kameezMeas.model';
import { KameezMeasurement } from '../../../../shared/model/kameezMeasurement.model';
import { BottomMeasurement } from '../../../../shared/model/bottomMeasurement.model';
import { MeasurementStyle } from '../../../../shared/model/measurementStyle.model';
import { MeasurementValue } from '../../../../shared/model/measurementValue.model';
import { AppSetting } from '../../../../config/appSetting';

@Component({
  selector: 'app-add-kameez-measurement-style',
  templateUrl: './add-kameez-measurement-style.component.html',
  styleUrls: ['./add-kameez-measurement-style.component.css']
})
export class AddKameezMeasurementStyleComponent implements OnInit {
  @ViewChild('backTitle', {static: false}) b;
  @ViewChild('frontTitle', {static: false}) f;
  @ViewChild('frontTitle', {static: false}) f1;
  @ViewChild('img1', {static: false}) i1;
  @ViewChild('img2', {static: false}) i2;
  @ViewChild('img3', {static: false}) i3;
  id: string;
  holder: any;
  measurementImageUrl;
  imageError1: boolean;
  fileToUpload: any;
  frontStyleImg: any[];
  reader: any;
  imageUploadStore: MeasurementImage = new MeasurementImage();
  imageData = {
    imageName: String
  };
  imageError2: boolean;
  backStyleImg: any[];
  sleeveStyleImg: any[];
  imageError3: boolean;
  bottomStyleImg: any[];

  constructor(private activatedRoute: ActivatedRoute, private productService: ProductService,
              private router: Router) {
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get('id');
    });
    // this.measurementImageUrl = AppSetting.measurementImageUrl;
    this.getSingleMeasurement();
   }

  ngOnInit() {
  }
  getSingleMeasurement() {
    this.productService.getSingleKameezMeasurement(this.id).subscribe(data => {
      this.holder = data;
    }, error => {
      console.log(error);
    });
  }
  frontNeckStyleImageUpload(images: any) {
    this.imageError1 = false;
    this.fileToUpload = images;
    this.imageUploadStore.frontNeckStyleImage = this.fileToUpload[0];
    this.frontStyleImg = [];
    const files = images;
    if (files) {
      for (const file of files) {
        this.reader = new FileReader();
        this.reader.onload = (e: any) => {
          this.frontStyleImg.push(e.target.result);
        };
        this.reader.readAsDataURL(file);
      }
    }
  }
  addFront(title) {
    const holder = new KameezMeasure();
    const subHolder = new MeasurementStyle();
    const store = new KameezMeasurement();
    subHolder.title = title;
    holder.frontNeckStyle = [subHolder];
   /*  store.kameezMeasurement = [holder]; */
    this.productService.addKameezFrontStyleMeasurement(holder, this.id).subscribe(data => {
      console.log(data);
      const length = data.kameezMeasurement[0].frontNeckStyle.length - 1;
      const id = data.kameezMeasurement[0].frontNeckStyle[length]._id;
      this.uploadFrontStyleImage(id);
      this.f.nativeElement.value = "";
      this.i1.nativeElement.src = "";
    }, error => {
      console.log(error);
    });
  }
  uploadFrontStyleImage(styleId) {
    if (this.imageUploadStore.frontNeckStyleImage !== undefined && this.imageUploadStore.frontNeckStyleImage !== null) {
      const formData: any = new FormData();
      formData.append('single', this.imageUploadStore.frontNeckStyleImage);
      this.productService.uploadImageForMeasurement(formData, this.id).subscribe(data => {
        this.updateFrontStyleImageName(data.Key, styleId);
      }, error => {
        console.log(error);
      });
    } else {
      this.getSingleMeasurement();
    }
  }
  updateFrontStyleImageName(name, styleId) {
    this.imageData = {
      imageName: name
    };
    this.productService.addKameezFrontStyleImageName(this.imageData, this.id, styleId).subscribe(data => {
      console.log(data);
      this.holder = data;
    }, error => {
      console.log(error);
    });
  }
  addBack(title) {
    const holder = new KameezMeasure();
    const subHolder = new MeasurementStyle();
    subHolder.title = title;
    holder.backNeckStyle = [subHolder];
    this.productService.addKameezBackStyleMeasurement(holder, this.id).subscribe(data => {
      console.log(data);
      const length = data.kameezMeasurement[0].backNeckStyle.length - 1;
      const id = data.kameezMeasurement[0].backNeckStyle[length]._id;
      this.uploadBackStyleImage(id);
    /*   this.b.nativeElement.value = "";
      this.i2.nativeElement.src = ""; */
    }, error => {
      console.log(error);
    });
  }
  backNeckStyleImageUpload(images: any) {
    this.imageError2 = false;
    this.fileToUpload = images;
    this.imageUploadStore.backNeckStyleImage = this.fileToUpload[0];
    this.backStyleImg = [];
    const files = images;
    if (files) {
      for (const file of files) {
        this.reader = new FileReader();
        this.reader.onload = (e: any) => {
          this.backStyleImg.push(e.target.result);
        };
        this.reader.readAsDataURL(file);
      }
    }
  }
  uploadBackStyleImage(styleId) {
    if (this.imageUploadStore.backNeckStyleImage !== undefined && this.imageUploadStore.backNeckStyleImage !== null) {
      const formData: any = new FormData();
      formData.append('single', this.imageUploadStore.backNeckStyleImage);
      this.productService.uploadImageForMeasurement(formData, this.id).subscribe(data => {
        this.updateBackStyleImageName(data.Key, styleId);
      }, error => {
        console.log(error);
      });
    } else {
      this.getSingleMeasurement();
    }
  }
  updateBackStyleImageName(name, styleId) {
    this.imageData = {
      imageName: name
    };
    this.productService.addKameezBackStyleImageName(this.imageData, this.id, styleId).subscribe(data => {
      console.log(data);
      this.holder = data;
    }, error => {
      console.log(error);
    });
  }

  addSleeve(title) {
    const holder = new KameezMeasure();
    const subHolder = new MeasurementStyle();
    subHolder.title = title;
    holder.sleeveStyle = [subHolder];
    this.productService.addKameezSleeveStyleMeasurement(holder, this.id).subscribe(data => {
      console.log(data);
      const length = data.kameezMeasurement[0].sleeveStyle.length - 1;
      const id = data.kameezMeasurement[0].sleeveStyle[length]._id;
      this.uploadSleeveStyleImage(id);
      this.f1.nativeElement.value = "";
      this.i3.nativeElement.src = "";
    }, error => {
      console.log(error);
    });
  }
  sleeveStyleImageUpload(images: any) {
    this.imageError3 = false;
    this.fileToUpload = images;
    this.imageUploadStore.sleeveNeckStyleImage = this.fileToUpload[0];
    this.sleeveStyleImg = [];
    const files = images;
    if (files) {
      for (const file of files) {
        this.reader = new FileReader();
        this.reader.onload = (e: any) => {
          this.sleeveStyleImg.push(e.target.result);
        };
        this.reader.readAsDataURL(file);
      }
    }
  }
  uploadSleeveStyleImage(styleId) {
    if (this.imageUploadStore.sleeveNeckStyleImage !== undefined && this.imageUploadStore.sleeveNeckStyleImage !== null) {
      const formData: any = new FormData();
      formData.append('single', this.imageUploadStore.sleeveNeckStyleImage);
      this.productService.uploadImageForMeasurement(formData, this.id).subscribe(data => {
        this.updateSleeveStyleImageName(data.Key, styleId);
      }, error => {
        console.log(error);
      });
    } else {
      this.getSingleMeasurement();
    }
  }
  updateSleeveStyleImageName(name, styleId) {
    this.imageData = {
      imageName: name
    };
    this.productService.addKameezSleeveStyleImageName(this.imageData, this.id, styleId).subscribe(data => {
      console.log(data);
      this.holder = data;
    }, error => {
      console.log(error);
    });
  }
  getView() {
    this.router.navigate(['product/viewMeasurement']);
  }
  removeFront(styleId) {
    this.productService.deleteKameezFrontNeckStyle(this.id, styleId).subscribe(data => {
      this.holder = data;
    }, error => {
      console.log(error);
    });
  }
  removeBack(styleId) {
    this.productService.deleteKameezBackNeckStyle(this.id, styleId).subscribe(data => {
      this.holder = data;
    }, error => {
      console.log(error);
    });
  }
  removeSleeve(styleId) {
    this.productService.deleteKameezSleeveLengthStyle(this.id, styleId).subscribe(data => {
      this.holder = data;
    }, error => {
      console.log(error);
    });
  }
  removeBottom(styleId) {
    this.productService.deleteKameezBottomStyle(this.id, styleId).subscribe(data => {
      this.holder = data;
    }, error => {
      console.log(error);
    });
  }
  addBottom(title) {
    const holder = new BottomMeasurement();
    const subHolder = new MeasurementStyle();
    subHolder.title = title;
    holder.bottomStyle = [subHolder];
    this.productService.addKameezBottomStyleMeasurement(holder, this.id).subscribe(data => {
      console.log(data);
      const length = data.bottomMeasurement[0].bottomStyle.length - 1;
      const id = data.bottomMeasurement[0].bottomStyle[length]._id;
      this.uploadBottomStyleImage(id);
      /* this.f1.nativeElement.value = "";
      this.i3.nativeElement.src = ""; */
    }, error => {
      console.log(error);
    });
  }
  bottomStyleImageUpload(images: any) {
    this.imageError3 = false;
    this.fileToUpload = images;
    this.imageUploadStore.bottomStyleImage = this.fileToUpload[0];
    this.bottomStyleImg = [];
    const files = images;
    if (files) {
      for (const file of files) {
        this.reader = new FileReader();
        this.reader.onload = (e: any) => {
          this.bottomStyleImg.push(e.target.result);
        };
        this.reader.readAsDataURL(file);
      }
    }
  }
  uploadBottomStyleImage(styleId) {
    if (this.imageUploadStore.bottomStyleImage !== undefined && this.imageUploadStore.bottomStyleImage !== null) {
      const formData: any = new FormData();
      formData.append('single', this.imageUploadStore.bottomStyleImage);
      this.productService.uploadImageForMeasurement(formData, this.id).subscribe(data => {
        this.updateBottomStyleImageName(data.Key, styleId);
      }, error => {
        console.log(error);
      });
    } else {
      this.getSingleMeasurement();
    }
  }
  updateBottomStyleImageName(name, styleId) {
    this.imageData = {
      imageName: name
    };
    this.productService.addKameezBottomStyleImageName(this.imageData, this.id, styleId).subscribe(data => {
      console.log(data);
      this.holder = data;
    }, error => {
      console.log(error);
    });
  }
}
