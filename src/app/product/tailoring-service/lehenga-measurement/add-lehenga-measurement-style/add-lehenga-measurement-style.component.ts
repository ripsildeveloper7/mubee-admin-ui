import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { ProductService } from '../../../product.service';
import { AppSetting } from '../../../../config/appSetting';
import { MeasurementImage } from '../create-lehenga-measurement/measurementImage.model';
import { LehengaMeasurement } from '../../../../shared/model/lehengaMeasurement.model';
import { LehengaMeasure } from '../../../../shared/model/lehengaMeasure.model';
import { CholiMeasurement } from '../../../../shared/model/choliMeasurement..model';
import { MeasurementStyle } from 'src/app/shared/model/measurementStyle.model';

@Component({
  selector: 'app-add-lehenga-measurement-style',
  templateUrl: './add-lehenga-measurement-style.component.html',
  styleUrls: ['./add-lehenga-measurement-style.component.css']
})
export class AddLehengaMeasurementStyleComponent implements OnInit {
  id: string;
  measurementImageUrl: string;
  holder: any;
  imageError1: boolean;
  fileToUpload: any;
  imageUploadStore: MeasurementImage = new MeasurementImage();
  frontStyleImg: any[];
  reader: FileReader;
  imageData = {
    imageName: String
  };
  imageError2: boolean;
  backStyleImg: any[];
  imageError3: boolean;
  sleeveStyleImg: any[];

  constructor(private activatedRoute: ActivatedRoute, private productService: ProductService,
              private router: Router) {
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get('id');
    });
    // this.measurementImageUrl = AppSetting.measurementImageUrl;
    this.getSingleMeasurement();
   }

  ngOnInit() {
  }
  getSingleMeasurement() {
    this.productService.getSingleLehengaMeasurement(this.id).subscribe(data => {
      this.holder = data;
    }, error => {
      console.log(error);
    });
  }
  frontNeckStyleImageUpload(images: any) {
    this.imageError1 = false;
    this.fileToUpload = images;
    this.imageUploadStore.frontNeckStyleImage = this.fileToUpload[0];
    this.frontStyleImg = [];
    const files = images;
    if (files) {
      for (const file of files) {
        this.reader = new FileReader();
        this.reader.onload = (e: any) => {
          this.frontStyleImg.push(e.target.result);
        };
        this.reader.readAsDataURL(file);
      }
    }
  }
  addFront(title) {
    const holder = new CholiMeasurement();
    const subHolder = new MeasurementStyle();
    const store = new LehengaMeasurement();
    subHolder.title = title;
    holder.frontNeckStyle = [subHolder];
   /*  store.kameezMeasurement = [holder]; */
    this.productService.addLehengaFrontStyleMeasurement(holder, this.id).subscribe(data => {
      console.log(data);
      const length = data.choliMeasurement[0].frontNeckStyle.length - 1;
      const id = data.choliMeasurement[0].frontNeckStyle[length]._id;
      this.uploadFrontStyleImage(id);
/*       this.f.nativeElement.value = "";
      this.i1.nativeElement.src = ""; */
    }, error => {
      console.log(error);
    });
  }
  uploadFrontStyleImage(styleId) {
    if (this.imageUploadStore.frontNeckStyleImage !== undefined && this.imageUploadStore.frontNeckStyleImage !== null) {
      const formData: any = new FormData();
      formData.append('single', this.imageUploadStore.frontNeckStyleImage);
      this.productService.uploadImageForMeasurement(formData, this.id).subscribe(data => {
        this.updateFrontStyleImageName(data.Key, styleId);
      }, error => {
        console.log(error);
      });
    } else {
      this.getSingleMeasurement();
    }
  }
  updateFrontStyleImageName(name, styleId) {
    this.imageData = {
      imageName: name
    };
    this.productService.addLehengaFrontStyleImageName(this.imageData, this.id, styleId).subscribe(data => {
      console.log(data);
      this.holder = data;
    }, error => {
      console.log(error);
    });
  }
  addBack(title) {
    const holder = new CholiMeasurement();
    const subHolder = new MeasurementStyle();
    subHolder.title = title;
    holder.backNeckStyle = [subHolder];
    this.productService.addLehengaBackStyleMeasurement(holder, this.id).subscribe(data => {
      console.log(data);
      const length = data.choliMeasurement[0].backNeckStyle.length - 1;
      const id = data.choliMeasurement[0].backNeckStyle[length]._id;
      this.uploadBackStyleImage(id);
    /*   this.b.nativeElement.value = "";
      this.i2.nativeElement.src = ""; */
    }, error => {
      console.log(error);
    });
  }
  backNeckStyleImageUpload(images: any) {
    this.imageError2 = false;
    this.fileToUpload = images;
    this.imageUploadStore.backNeckStyleImage = this.fileToUpload[0];
    this.backStyleImg = [];
    const files = images;
    if (files) {
      for (const file of files) {
        this.reader = new FileReader();
        this.reader.onload = (e: any) => {
          this.backStyleImg.push(e.target.result);
        };
        this.reader.readAsDataURL(file);
      }
    }
  }
  uploadBackStyleImage(styleId) {
    if (this.imageUploadStore.backNeckStyleImage !== undefined && this.imageUploadStore.backNeckStyleImage !== null) {
      const formData: any = new FormData();
      formData.append('single', this.imageUploadStore.backNeckStyleImage);
      this.productService.uploadImageForMeasurement(formData, this.id).subscribe(data => {
        this.updateBackStyleImageName(data.Key, styleId);
      }, error => {
        console.log(error);
      });
    } else {
      this.getSingleMeasurement();
    }
  }
  updateBackStyleImageName(name, styleId) {
    this.imageData = {
      imageName: name
    };
    this.productService.addLehengaBackStyleImageName(this.imageData, this.id, styleId).subscribe(data => {
      console.log(data);
      this.holder = data;
    }, error => {
      console.log(error);
    });
  }

  addSleeve(title) {
    const holder = new CholiMeasurement();
    const subHolder = new MeasurementStyle();
    subHolder.title = title;
    holder.sleeveStyle = [subHolder];
    this.productService.addLehengaSleeveStyleMeasurement(holder, this.id).subscribe(data => {
      console.log(data);
      const length = data.choliMeasurement[0].sleeveStyle.length - 1;
      const id = data.choliMeasurement[0].sleeveStyle[length]._id;
      this.uploadSleeveStyleImage(id);
      /* this.f1.nativeElement.value = "";
      this.i3.nativeElement.src = ""; */
    }, error => {
      console.log(error);
    });
  }
  sleeveStyleImageUpload(images: any) {
    this.imageError3 = false;
    this.fileToUpload = images;
    this.imageUploadStore.sleeveNeckStyleImage = this.fileToUpload[0];
    this.sleeveStyleImg = [];
    const files = images;
    if (files) {
      for (const file of files) {
        this.reader = new FileReader();
        this.reader.onload = (e: any) => {
          this.sleeveStyleImg.push(e.target.result);
        };
        this.reader.readAsDataURL(file);
      }
    }
  }
  uploadSleeveStyleImage(styleId) {
    if (this.imageUploadStore.sleeveNeckStyleImage !== undefined && this.imageUploadStore.sleeveNeckStyleImage !== null) {
      const formData: any = new FormData();
      formData.append('single', this.imageUploadStore.sleeveNeckStyleImage);
      this.productService.uploadImageForMeasurement(formData, this.id).subscribe(data => {
        this.updateSleeveStyleImageName(data.Key, styleId);
      }, error => {
        console.log(error);
      });
    } else {
      this.getSingleMeasurement();
    }
  }
  updateSleeveStyleImageName(name, styleId) {
    this.imageData = {
      imageName: name
    };
    this.productService.addLehengaSleeveStyleImageName(this.imageData, this.id, styleId).subscribe(data => {
      console.log(data);
      this.holder = data;
    }, error => {
      console.log(error);
    });
  }

  removeFront(styleId) {
    this.productService.removeLehengaFrontNeckStyleImage(this.id, styleId).subscribe(data => {
      this.holder = data;
    }, error => {
      console.log(error);
    });
  }
  removeBack(styleId) {
    this.productService.removeLehengaBackNeckStyleImage(this.id, styleId).subscribe(data => {
      this.holder = data;
    }, error => {
      console.log(error);
    });
  }
  removeSleeve(styleId) {
    this.productService.removeLehengaSleeveStyleImage(this.id, styleId).subscribe(data => {
      this.holder = data;
    }, error => {
      console.log(error);
    });
  }
  getView() {
    this.router.navigate(['product/viewlehengameasurement']);
  }
}
