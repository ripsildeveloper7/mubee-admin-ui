import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../../product.service';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { MeasurementImage } from './measurementImage.model';
import { MeasurementValue } from '../../../../shared/model/measurementValue.model';
import { LehengaMeasurement } from '../../../../shared/model/lehengaMeasurement.model';
import { LehengaMeasure } from '../../../../shared/model/lehengaMeasure.model';
import { CholiMeasurement } from '../../../../shared/model/choliMeasurement..model';


@Component({
  selector: 'app-create-lehenga-measurement',
  templateUrl: './create-lehenga-measurement.component.html',
  styleUrls: ['./create-lehenga-measurement.component.css']
})
export class CreateLehengaMeasurementComponent implements OnInit {
  superCategoryModel: any;
  measurementForm: FormGroup;
  imageError: boolean;
  fileToUpload: any;
  imageUploadStore: MeasurementImage = new MeasurementImage();
  aroundBustImg: any[];
  reader: FileReader;
  imageError1: boolean;
  aroundAboveWaistImg: any[];
  choliLengthImg: any[];
  imageError2: boolean;
  imageError3: boolean;
  aroundWaistImg: any[];
  imageError4: boolean;
  aroundHipImg: any[];
  imageError5: boolean;
  lehengaLengthImg: any[];
  imageData = {
    imageName: String
  };

  constructor(private productService: ProductService, private fb: FormBuilder, private router: Router) { 
    this.showSuperCategory();
  }

  ngOnInit() {
    this.createForm();
  }
  createForm() {
    this.measurementForm = this.fb.group({
      superCategoryId: [''],
      measurementName: [''],
      amount: [''],
      discount: [''],
      measurementType: this.fb.array([]),
      aroundBustMin: [''],
      aroundBustMax: [''],
      aroundBustDescription: [''],
      aroundAboveWaistMin: [''],
      aroundAboveWaistMax: [''],
      aroundAboveWaistDescription: [''],
      choliLengthMin: [''],
      choliLengthMax: [''],
      choliLengthDescription: [''],
      aroundWaistMin: [''],
      aroundWaistMax: [''],
      aroundWaistDescription: [''],
      aroundHipMin: [''],
      aroundHipMax: [''],
      aroundHipDescription: [''],
      lehengaLengthMin: [''],
      lehengaLengthMax: [''],
      lehengaLengthDescription: [''],
      choliClosingSide: this.fb.array([]),
      choliClosingWith: this.fb.array([]),
      lining: this.fb.array([]),
      lehengaClosingSide: this.fb.array([]),
      lehengaClosingWith: this.fb.array([])
    });
  }
  showSuperCategory() {
    this.productService.getSuperCategory().subscribe(data => {
      this.superCategoryModel = data;
    }, err => {
      console.log(err);
    });
  }
  addTypeForm() {
    const measurementType = this.fb.group({
      typeName: [''],
      typeDescription: ['']
    });
    this.typeForms.push(measurementType);
  }
  get typeForms() {
    console.log(this.measurementForm);
    return this.measurementForm.get('measurementType') as FormArray;
  }
  deleteType(i) {
    this.typeForms.removeAt(i);
  }
  addCholiClosingSideForm() {
    const choliClosingSide = this.fb.group({
      choliClosingSide: ['']
    });
    this.choliClosingSideForms.push(choliClosingSide);
  }
  get choliClosingSideForms() {
    return this.measurementForm.get('choliClosingSide') as FormArray;
  }
  deleteCholiClosingSide(i) {
    this.choliClosingSideForms.removeAt(i);
  }
  addCholiClosingWithForm() {
    const choliClosingWith = this.fb.group({
      choliClosingWith: ['']
    });
    this.choliClosingWithForms.push(choliClosingWith);
  }
  get choliClosingWithForms() {
    return this.measurementForm.get('choliClosingWith') as FormArray;
  }
  deleteCholiClosingWith(i) {
    this.choliClosingWithForms.removeAt(i);
  }
  addLiningForm() {
    const lining = this.fb.group({
      lining: ['']
    });
    this.liningForms.push(lining);
  }
  get liningForms() {
    return this.measurementForm.get('lining') as FormArray;
  }
  deleteLining(i) {
    this.liningForms.removeAt(i);
  }
  addLehengaClosingWithForm() {
    const lehengaClosingWith = this.fb.group({
      lehengaClosingWith: ['']
    });
    this.lehengaClosingWithForms.push(lehengaClosingWith);
  }
  get lehengaClosingWithForms() {
    return this.measurementForm.get('lehengaClosingWith') as FormArray;
  }
  deleteLehengaClosingWith(i) {
    this.lehengaClosingWithForms.removeAt(i);
  }
  addClosingSideForm() {
    const lehengaClosingSide = this.fb.group({
      lehengaClosingSide: ['']
    });
    this.lehengaClosingSideForms.push(lehengaClosingSide);
  }
  get lehengaClosingSideForms() {
    return this.measurementForm.get('lehengaClosingSide') as FormArray;
  }
  deleteClosingSide(i) {
    this.lehengaClosingSideForms.removeAt(i);
  }
  aroundBustImageUpload(images: any) {
    this.imageError = false;
    this.fileToUpload = images;
    this.imageUploadStore.aroundBustImage = this.fileToUpload[0];
    this.aroundBustImg = [];
    const files = images;
    if (files) {
      for (const file of files) {
        this.reader = new FileReader();
        this.reader.onload = (e: any) => {
          this.aroundBustImg.push(e.target.result);
        };
        this.reader.readAsDataURL(file);
      }
    }
  }
  aroundAboveWaistImageUpload(images: any) {
    this.imageError1 = false;
    this.fileToUpload = images;
    this.imageUploadStore.aroundAboveWaistImage = this.fileToUpload[0];
    this.aroundAboveWaistImg = [];
    const files = images;
    if (files) {
      for (const file of files) {
        this.reader = new FileReader();
        this.reader.onload = (e: any) => {
          this.aroundAboveWaistImg.push(e.target.result);
        };
        this.reader.readAsDataURL(file);
      }
    }
  }
  choliLengthImageUpload(images: any) {
    this.imageError2 = false;
    this.fileToUpload = images;
    this.imageUploadStore.choliLengthImage = this.fileToUpload[0];
    this.choliLengthImg = [];
    const files = images;
    if (files) {
      for (const file of files) {
        this.reader = new FileReader();
        this.reader.onload = (e: any) => {
          this.choliLengthImg.push(e.target.result);
        };
        this.reader.readAsDataURL(file);
      }
    }
  }
  aroundWaistImageUpload(images: any) {
    this.imageError3 = false;
    this.fileToUpload = images;
    this.imageUploadStore.aroundwaistImage = this.fileToUpload[0];
    this.aroundWaistImg = [];
    const files = images;
    if (files) {
      for (const file of files) {
        this.reader = new FileReader();
        this.reader.onload = (e: any) => {
          this.aroundWaistImg.push(e.target.result);
        };
        this.reader.readAsDataURL(file);
      }
    }
  }
  aroundHipImageUpload(images: any) {
    this.imageError4 = false;
    this.fileToUpload = images;
    this.imageUploadStore.aroundHipImage = this.fileToUpload[0];
    this.aroundHipImg = [];
    const files = images;
    if (files) {
      for (const file of files) {
        this.reader = new FileReader();
        this.reader.onload = (e: any) => {
          this.aroundHipImg.push(e.target.result);
        };
        this.reader.readAsDataURL(file);
      }
    }
  }
  lehengaLengthImageUpload(images: any) {
    this.imageError5 = false;
    this.fileToUpload = images;
    this.imageUploadStore.lehengaLengthImage = this.fileToUpload[0];
    this.lehengaLengthImg = [];
    const files = images;
    if (files) {
      for (const file of files) {
        this.reader = new FileReader();
        this.reader.onload = (e: any) => {
          this.lehengaLengthImg.push(e.target.result);
        };
        this.reader.readAsDataURL(file);
      }
    }
  }
  onSubmit() {
    const measurementModel = new LehengaMeasurement();
    measurementModel.measurementName = this.measurementForm.controls.measurementName.value;
    measurementModel.measurementType = this.measurementForm.controls.measurementType.value;
    measurementModel.price = this.measurementForm.controls.amount.value;
    measurementModel.discount = this.measurementForm.controls.discount.value;
    measurementModel.superCategoryId = this.measurementForm.controls.superCategoryId.value;
    const aroundBust = new MeasurementValue();
    aroundBust.min = this.measurementForm.controls.aroundBustMin.value;
    aroundBust.max = this.measurementForm.controls.aroundBustMax.value;
    aroundBust.description = this.measurementForm.controls.aroundBustDescription.value;
    const aroundAboveWaist = new MeasurementValue();
    aroundAboveWaist.min = this.measurementForm.controls.aroundAboveWaistMin.value;
    aroundAboveWaist.max = this.measurementForm.controls.aroundAboveWaistMax.value;
    aroundAboveWaist.description = this.measurementForm.controls.aroundAboveWaistDescription.value;
    const aroundHip = new MeasurementValue();
    aroundHip.min = this.measurementForm.controls.aroundHipMin.value;
    aroundHip.max = this.measurementForm.controls.aroundHipMax.value;
    aroundHip.description = this.measurementForm.controls.aroundHipDescription.value;
    const choliLength = new MeasurementValue();
    choliLength.min = this.measurementForm.controls.choliLengthMin.value;
    choliLength.max = this.measurementForm.controls.choliLengthMax.value;
    choliLength.description = this.measurementForm.controls.choliLengthDescription.value;
    const choli = new CholiMeasurement();
    choli.choliClosingSide = this.measurementForm.controls.choliClosingSide.value.map(e => e.choliClosingSide);
    choli.choliClosingWith = this.measurementForm.controls.choliClosingWith.value.map(e => e.choliClosingWith);
    choli.lining = this.measurementForm.controls.lining.value.map(e => e.lining);
    choli.aroundBust = [aroundBust];
    choli.aroundAboveWaist = [aroundAboveWaist];
    choli.choliLength = [choliLength];
    const aroundWaist = new MeasurementValue();
    aroundWaist.min = this.measurementForm.controls.aroundWaistMin.value;
    aroundWaist.max = this.measurementForm.controls.aroundWaistMax.value;
    aroundWaist.description = this.measurementForm.controls.aroundWaistDescription.value;
    const lehengaLength = new MeasurementValue();
    lehengaLength.min = this.measurementForm.controls.lehengaLengthMin.value;
    lehengaLength.max = this.measurementForm.controls.lehengaLengthMax.value;
    lehengaLength.description = this.measurementForm.controls.lehengaLengthDescription.value;
    const lehengaMeasure = new LehengaMeasure();
    lehengaMeasure.lehengaClosingSide = this.measurementForm.controls.lehengaClosingSide.value.map(e => e.lehengaClosingSide);
    lehengaMeasure.lehengaClosingWith = this.measurementForm.controls.lehengaClosingWith.value.map(e => e.lehengaClosingWith);
    lehengaMeasure.aroundWaist = [aroundWaist];
    lehengaMeasure.aroundHip = [aroundHip];
    lehengaMeasure.lehengaLength = [lehengaLength];
    measurementModel.choliMeasurement = [choli];
    measurementModel.lehengaMeasurement = [lehengaMeasure];
   /*  console.log(measurementModel); */
    this.productService.createLehengaMeasurement(measurementModel).subscribe(data => {
      console.log(data);
      this.uploadAroundBustImage(data._id);
    }, error => {
      console.log(error);
    });
  }
  uploadAroundBustImage(id) {
    if (this.imageUploadStore.aroundBustImage !== undefined && this.imageUploadStore.aroundBustImage !== null) {
      const formData: any = new FormData();
      formData.append('single', this.imageUploadStore.aroundBustImage);
      this.productService.uploadImageForMeasurement(formData, id).subscribe(data => {
        this.updateAroundBustImageName(data.Key, id);
      }, error => {
        console.log(error);
      });
    } else {
      this.uploadAroundAboveWaistImage(id);
    }
  }
  updateAroundBustImageName(name, id) {
    this.imageData = {
      imageName: name
    };
    this.productService.addLehengaAroundBustImageName(this.imageData, id).subscribe(data => {
      console.log(data);
      this.uploadAroundAboveWaistImage(id);
    }, error => {
      console.log(error);
    });
  }
  uploadAroundAboveWaistImage(id) {
    if (this.imageUploadStore.aroundAboveWaistImage !== undefined && this.imageUploadStore.aroundAboveWaistImage !== null) {
      const formData: any = new FormData();
      formData.append('single', this.imageUploadStore.aroundAboveWaistImage);
      this.productService.uploadImageForMeasurement(formData, id).subscribe(data => {
        this.updateAroundAboveWaiseImageName(data.Key, id);
      }, error => {
        console.log(error);
      });
    } else {
      this.uploadCholiLengthImage(id);
    }
  }
  updateAroundAboveWaiseImageName(name, id) {
    this.imageData = {
      imageName: name
    };
    this.productService.addLehengaAroundAboveWaistImageName(this.imageData, id).subscribe(data => {
      console.log(data);
      this.uploadCholiLengthImage(id);
    }, error => {
      console.log(error);
    });
  }

  uploadCholiLengthImage(id) {
    if (this.imageUploadStore.choliLengthImage !== undefined && this.imageUploadStore.choliLengthImage !== null) {
      const formData: any = new FormData();
      formData.append('single', this.imageUploadStore.choliLengthImage);
      this.productService.uploadImageForMeasurement(formData, id).subscribe(data => {
        this.updateCholiLengthImageName(data.Key, id);
      }, error => {
        console.log(error);
      });
    } else {
      this.uploadAroundWaistImage(id);
    }
  }
  updateCholiLengthImageName(name, id) {
    this.imageData = {
      imageName: name
    };
    this.productService.addLehengaColiLengthImageName(this.imageData, id).subscribe(data => {
      console.log(data);
      this.uploadAroundWaistImage(id);
    }, error => {
      console.log(error);
    });
  }
  uploadAroundWaistImage(id) {
    if (this.imageUploadStore.aroundwaistImage !== undefined && this.imageUploadStore.aroundwaistImage !== null) {
      const formData: any = new FormData();
      formData.append('single', this.imageUploadStore.aroundwaistImage);
      this.productService.uploadImageForMeasurement(formData, id).subscribe(data => {
        this.updateAroundWaistImageName(data.Key, id);
      }, error => {
        console.log(error);
      });
    } else {
      this.uploadAroundHipImage(id);
    }
  }
  updateAroundWaistImageName(name, id) {
    this.imageData = {
      imageName: name
    };
    this.productService.addLehengaAroundWaistImageName(this.imageData, id).subscribe(data => {
      console.log(data);
      this.uploadAroundHipImage(id);
    }, error => {
      console.log(error);
    });
  }
  uploadAroundHipImage(id) {
    if (this.imageUploadStore.aroundHipImage !== undefined && this.imageUploadStore.aroundHipImage !== null) {
      const formData: any = new FormData();
      formData.append('single', this.imageUploadStore.aroundHipImage);
      this.productService.uploadImageForMeasurement(formData, id).subscribe(data => {
        this.updateAroundHipImageName(data.Key, id);
      }, error => {
        console.log(error);
      });
    } else {
      this.uploadLehengaLengthImage(id);
    }
  }
  updateAroundHipImageName(name, id) {
    this.imageData = {
      imageName: name
    };
    this.productService.addLehengaAroundHipImageName(this.imageData, id).subscribe(data => {
      console.log(data);
      this.uploadLehengaLengthImage(id);
    }, error => {
      console.log(error);
    });
  }
  uploadLehengaLengthImage(id) {
    if (this.imageUploadStore.lehengaLengthImage !== undefined && this.imageUploadStore.lehengaLengthImage !== null) {
      const formData: any = new FormData();
      formData.append('single', this.imageUploadStore.lehengaLengthImage);
      this.productService.uploadImageForMeasurement(formData, id).subscribe(data => {
        this.updateLehengaLengthImageName(data.Key, id);
      }, error => {
        console.log(error);
      });
    } else {
      this.router.navigate(['product/viewlehengameasurement']);
    }
  }
  updateLehengaLengthImageName(name, id) {
    this.imageData = {
      imageName: name
    };
    this.productService.addLehengaLengthImageName(this.imageData, id).subscribe(data => {
      this.router.navigate(['product/viewlehengameasurement']);
    }, error => {
      console.log(error);
    });
  }
}
