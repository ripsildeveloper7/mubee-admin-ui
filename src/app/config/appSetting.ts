import {environment} from '../../environments/environment';

export const AppSetting: AppSettingType = {
    imageUploadServiceUrl: environment.imageUploadServiceUrl,
    productServiceUrl: environment.productServiceUrl,
    productImageUrl: environment.productImageUrl,
    categoryImageUrl: environment.categoryImageUrl,
    brandImageUrl: environment.brandImageUrl,
    // cmsServiceUrl: environment.cmsServiceUrl,
    commerceOrderServiceUrl: environment.commerceOrderServiceUrl,
    contentServiceUrl:environment.contentServiceUrl,
    // customerSerivceUrl: environment.customerServiceUrl,
    customerServiceUrl: environment.customerServiceUrl,
    categoryBannerImageUrl: environment.categoryBannerImageUrl,
   
    subCategoryImageUrl: environment.subCategoryImageUrl,
    sizeGuideImageUrl: environment.sizeGuideImageUrl,
    marketingServiceUrl: environment.marketingServiceUrl,
    // vendorImageServiceUrl: environment.vendorImageServiceUrl,
    excelUrl: environment.excelUrl,
    instagramUrl:environment.instagramUrl,
    appId: environment.appId,
    contentImageUrl: environment.contentImageUrl,
    mainCategoryBannerImageUrl: environment.mainCategoryBannerImageUrl,
    parentResizeId: environment.parentResizeId,
    childResizeId: environment.childResizeId,
    limitResize: environment.limitResize
    // measurementImageUrl: environment.marketingServiceUrl,
};