interface AppSettingType {
    customerServiceUrl: string;
    productServiceUrl: string;
    productImageUrl: string;
    brandImageUrl: string;
    commerceOrderServiceUrl: string;
    contentServiceUrl: string;
    subCategoryImageUrl: string;
    categoryImageUrl: string;
    sizeGuideImageUrl: string;
    instagramUrl: string;
    marketingServiceUrl: string;
    categoryBannerImageUrl: string;
    imageUploadServiceUrl: string;
    appId: string;
    excelUrl: string;
    mainCategoryBannerImageUrl: string;
    contentImageUrl: string;
    parentResizeId: number;
    childResizeId: number;
    limitResize: number
}