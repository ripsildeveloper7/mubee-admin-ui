import { SubCategory } from './../sub-category/sub-category.model';
import {FieldAttributeValue} from './../sub-category/field-attribute-value.model'
export class MainCategory {
    _id: string;
    mainCategoryName: string;
    mainCategoryDescription: string;
    mainCategoryNameError: boolean;
    subCategory: [SubCategory];
    attribute: [{fieldName: String, fieldType: String, fieldSetting: String, fieldValue: FieldAttributeValue}];
}
