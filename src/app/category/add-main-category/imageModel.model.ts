export class ImageModel {
    mainCategoryImage: File;
    image64: string | ArrayBuffer;
    image: string;
}
