export class ImageModel {
    subCategoryImage: File;
    image64: string | ArrayBuffer;
    image: string;
}
