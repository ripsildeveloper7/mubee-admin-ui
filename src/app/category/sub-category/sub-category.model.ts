import {FieldAttributeValue} from './field-attribute-value.model'
export class SubCategory {
    _id: string;
    subCategoryName: string;
    subCategoryImageName: string;
    image64: string | ArrayBuffer;
    subCategoryDescription: string;
    metaTagTitle: string;
    metaTagDescription: string;
    metaTagKeyword: string;
    attribute: [{fieldName: String, fieldType: String, fieldSetting: String, fieldValue: FieldAttributeValue}];
    isDispatch;
}
