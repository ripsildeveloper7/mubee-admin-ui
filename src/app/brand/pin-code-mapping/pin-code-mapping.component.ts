import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { BrandService } from './../brand.service';
import * as XLSX from 'xlsx';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { BrandModel } from './../add-brand/brand.model';
@Component({
  selector: 'app-pin-code-mapping',
  templateUrl: './pin-code-mapping.component.html',
  styleUrls: ['./pin-code-mapping.component.css']
})
export class PinCodeMappingComponent implements OnInit {
  brandModel: BrandModel[];
  pincodeDetails;
  file: File;
  arrayBuffer: any;
  pincodeMapping;
  disableUpload: boolean;
  excelPincodeData: any = [
    {
      City: '24 PARGANAS',
      Pincode: '700039',
      HasCod: 'TRUE',
      HasPrepaid: 'TRUE',
      HasReverse: 'TRUE',
      ShiprocketDeliveryZone: 'z_d',
      RoutingCode: 'BNR',
      State: 'WEST BENGAL',
      TAT: ''
    }
  ];
  id;
  constructor(private brandService: BrandService, private fb: FormBuilder, private router: Router,
    private route: ActivatedRoute, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.showAllBrand();
    this.getAllPincodeDetails();
    this.disableUpload = false;
  }
  downloadPincode() {
    this.brandService.exportAsExcelFile(this.excelPincodeData, 'PinCode excelFormat');
  }
  showAllBrand() {
    this.brandService.getAllBrand().subscribe(data => {
      this.brandModel = data;
    }, err => {
      console.log(err);
    })
  }
  selectedBrand(brand) {
    this.id = brand.value._id;
    var target = this.pincodeDetails.find(temp => temp.brandId == this.id);
    if (target !== undefined || target.length !== 0) {
      this.disableUpload = true;
    } else if (target === undefined) {
      this.disableUpload = false;
    }
  }
  uploadExcel() {
    const fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.arrayBuffer = fileReader.result;
      const data = new Uint8Array(this.arrayBuffer);
      const arr = new Array();
      for (let i = 0; i !== data.length; ++i) {
        arr[i] = String.fromCharCode(data[i]);
      }
      const bstr = arr.join('');
      const workbook = XLSX.read(bstr, { type: 'binary' });
      const first_sheet_name = workbook.SheetNames[0];
      const worksheet = workbook.Sheets[first_sheet_name];
      this.pincodeMapping = XLSX.utils.sheet_to_json(worksheet);
      this.brandService.uploadPincode(this.pincodeMapping, this.id)
        .subscribe(detail => {
          this.pincodeDetails = detail;
          console.log(detail, 'Pincode added successfully');
        }, error => {
          console.log(`Server Down please try again`);
          /* this.alertService.confirm({message: `Server Down please try again`}); */
          console.log(error);
        });
    };
    fileReader.readAsArrayBuffer(this.file);
  }
  uploadingPincodeMapping(event) {
    this.file = event.target.files[0];
  }
  getAllPincodeDetails() {
    this.brandService.getAllPincodeDetails().subscribe(data => {
      this.pincodeDetails = data;
      console.log(data, 'pincode Details');
    }, err => {
      console.log(err);
    })
  }

  // delete pincode
  deletePinCode(id) {
    this.brandService.deletePincode(id).subscribe(data => {
      this.pincodeDetails = data;
    }, err => {
      console.log(err);
    })
  }
}
