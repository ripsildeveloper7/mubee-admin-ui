export class BrandModel {
    _id: string;
    brandName: string;
    brandTitle: string;
    brandDescription: string;
    brandImageName: string;
    brandId: string;
    brandStatus: boolean;
    metaTitle: string;
    metaDescription: string;
    metaContent: string;
}
