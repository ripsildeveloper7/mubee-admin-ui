import { Component, OnInit } from '@angular/core';
import { BrandModel } from './../add-brand/brand.model';
import { BrandImageData } from './../add-brand/brandImage.model';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBar } from '@angular/material';
import { BrandService } from './../brand.service';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-edit-brand',
  templateUrl: './edit-brand.component.html',
  styleUrls: ['./edit-brand.component.css']
})
export class EditBrandComponent implements OnInit {
  brandForm: FormGroup;
  fileToUpload: any;
  brandImageData: BrandImageData = new BrandImageData();
  urls: any[];
  reader: FileReader;
  imageNameFilter: any;
  brandModel: any;
  showImageNameError: boolean;
  brand: BrandModel;
  message: string;
  fileLength: any;
  action: string;
  selectedStatus: boolean;
  selectStatus = false;
  activeStatus = [true, false];
  id: string;
  imageEdit = false;
  status;
  file: File;
  arrayBuffer: any;
  pincodeMapping;
  excelPincodeData: any = [
    {
      City: '24 PARGANAS',
      Pincode: '700039',
      HasCod: 'TRUE',
      HasPrepaid: 'TRUE',
      HasReverse: 'TRUE',
      ShiprocketDeliveryZone: 'z_d',
      RoutingCode: 'BNR',
      State: 'WEST BENGAL',
      TAT: ''
    }
  ];
  constructor(private brandService: BrandService, private fb: FormBuilder, private router: Router,
              private route: ActivatedRoute, private snackBar: MatSnackBar) {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get('id');
    });
  }

  ngOnInit() {
    this.getSelectedBrand();
  }
  getSelectedBrand() {
    this.brandService.getSingleBrand(this.id).subscribe(data => {
      this.brand = data;
      this.status = this.brand.brandStatus;
    }, error => {
      console.log(error);
    });
  }
  updateBrand(brandName, brandTitle, brandDescription, brandId, brandStatus, metaTitle, metaDescription, metaContent)   {
    this.brand = new BrandModel();
    this.brand.brandName =  brandName;
    this.brand.brandTitle =  brandTitle;
    this.brand.brandDescription =  brandDescription;
    this.brand.brandId =  brandId;
    this.brand.brandStatus =  brandStatus;
    this.brand.metaTitle =  metaTitle;
    this.brand.metaDescription =  metaDescription;
    this.brand.metaContent =  metaContent;
    this.brandService.updateSingleBrand(this.id, this.brand).subscribe(data => {
    this.brand = data;
    }, error => {
      console.log(error);
    });
  }
  cancelBrand()   {
    this.router.navigate(['brand/viewbrand']);
  }
  editImage() {
    this.imageEdit = true;
  }
  cancelImage() {
    this.imageEdit = false;
  }
  handleFileInput(images: any) {
    this.urls = [];
    this.fileToUpload = images;
    this.brandImageData.brandImage = this.fileToUpload[0];
    const files = images;
    if (files) {
      for (const file of files) {
        this.reader = new FileReader();
        this.reader.onload = (e: any) => {
          this.urls.push(e.target.result);
        };
        this.reader.readAsDataURL(file);
      }
    }
  }
  updateImage() {
    const formData: any = new FormData();
    this.fileLength = this.fileToUpload.length;
    for (let i = 0; i <= this.fileLength; i++) {
      formData.append('uploads[]', this.fileToUpload[i]);
    }
    this.brandService.uploadBrandImage(formData, this.id).subscribe(data => {
    this.brand = data;
    this.imageEdit = false;
    }, error => {
      console.log(error);
    });
  }
}
