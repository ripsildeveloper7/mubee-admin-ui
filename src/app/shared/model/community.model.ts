
export class Community {
    _id: string;
    heading: String;
    subHeading: String;
    description: String;
    bannerImageName: any;
    bannerImage: [{
        _id: string;
        showDiv: boolean;
        bannerImageName: String;

    }];
    communityDescription: [{
        _id: string;
        description: String;
        showDescription: boolean
    }];
    fileName: File;
    showHeadingDiv: boolean;
    showSubHeadingDiv: boolean
}
