import { ReferAndEarnCustomer } from './referAndEarnCustomer.model';

export class ReferAndEarn {
    _id: string;
    couponName: String;
    couponDescription: String;
    referrerPercentage: Number;
    applierPercetage: Number;
    countOfApplier: Number;
    customerCouponDetails: [ReferAndEarnCustomer];
    startDate: Date;
    endDate: Date;
    result: any;
    showDiv: boolean;
    dateError: boolean
}
