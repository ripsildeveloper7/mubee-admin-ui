export class ShiprocketOrder {
    order_id: string;
    shipment_id: string;
    status: string;
    status_code: Number;
    onboarding_completed_now: Number;
    orderObjectId: string;
    websiteOrderId: string;

}